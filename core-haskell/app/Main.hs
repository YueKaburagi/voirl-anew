module Main (main) where

import Lib

main :: IO ()
main = do
  argv <- getArgs
  case argv of
    [] -> mainMode
    ["gen-conf"] -> genDefaultConfigFile
    _ -> putStrLn "Error: Unexpected arguments."
