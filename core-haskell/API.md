
# API のメモ

## Top
`{ "cmd" : "keep-alive" }`
`{ "cmd" : "kill" }`

## System
`{ "type" : "continue" }`
`{ "type" : "new-game" }`
`{ "type" : "save" }`
`{ "type" : "list-all-save" }`
`{ "type" : "load", "index" : <index:text> }`
`{ "type" : "game", "game" : <obj> }`

### Game (上の <obj> の中身
`{ "cmd" : "count-up" }`
