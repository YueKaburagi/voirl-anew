{-# LANGUAGE OverloadedStrings #-}

module Response ( PackedResponse(..)
                , ResponseSchemeName(..)
                , ResponseBody(..)
                , Response(..)
                , CommonJSONResponse(..)
                , packResponse
                , isPlannedExit
                ) where

import Data.Aeson (ToJSON, toJSON, object, (.=), encode)
import qualified Data.ByteString.Lazy as L


newtype ResponseSchemeName = ResponseSchemeName ByteString
newtype ResponseBody = ResponseBody ByteString

data PackedResponse = PackedResponse ResponseSchemeName ResponseBody


packResponse :: ToJSON a => Response a -> PackedResponse
packResponse r = PackedResponse (responseScheme r) (responseBody r)

responseScheme :: Response a -> ResponseSchemeName
responseScheme (ResponseRaw _) = ResponseSchemeName "binary"
responseScheme (ResponseText _) = ResponseSchemeName "plain-text"
responseScheme (ResponseJSON _) = ResponseSchemeName "json"
responseScheme (ResponseJSONCommon _) = ResponseSchemeName "json"

responseBody :: ToJSON a => Response a -> ResponseBody
responseBody (ResponseRaw bs) = ResponseBody bs
responseBody (ResponseText t) = ResponseBody $ encodeUtf8 t
responseBody (ResponseJSON v) = ResponseBody $ L.toStrict $ encode v
responseBody (ResponseJSONCommon v) = ResponseBody $ L.toStrict $ encode v

data Response a = ResponseRaw ByteString
                | ResponseText Text
                | ResponseJSON a
                | ResponseJSONCommon CommonJSONResponse
                deriving (Eq)

data CommonJSONResponse = CJResponseContinue
                        | CJSucceed
                        | CJFailure
                        | CJReceiveExit
                        | CJError Text
                        | CJKnownSavefiles [Text]
                        deriving (Eq)
instance ToJSON CommonJSONResponse where
  toJSON CJResponseContinue = object [ "type" .= ("request-continue" :: Text) ]
  toJSON CJSucceed = object [ "type" .= ("succeed" :: Text) ]
  toJSON CJFailure = object [ "type" .= ("failure" :: Text) ]
  toJSON CJReceiveExit = object [ "type" .= ("receive-exit" :: Text) ]
  toJSON (CJError t) = object [ "type" .= ("error" :: Text)
                              , "err" .= t ]
  toJSON (CJKnownSavefiles files) = object [ "type" .= ("savefiles" :: Text)
                                           , "indices" .= files
                                           ]



-- |  Game が自己終了しようとしているかを見る
isPlannedExit :: Response r -> Bool
isPlannedExit (ResponseJSONCommon CJReceiveExit) = True
isPlannedExit _ = False

