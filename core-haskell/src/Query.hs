{-# LANGUAGE OverloadedStrings,LambdaCase #-}

module Query ( Query(..)
             , SystemQuery(..)
             , bytestringToQuery
             , isValidQuery
             ) where

import Data.Aeson (FromJSON, parseJSON, withObject, (.:), decodeStrict)
import Data.Aeson.Types (Parser)


data Query a = QueryKeepAlive
             | QueryKill
             | QuerySystem (SystemQuery a)
             | QueryUnknown
             | QueryUnexpected
             deriving (Eq)

data SystemQuery a = QueryContinue
                   | QueryCreateNewData
                   | QueryListAllSavedata
                   | QueryLoadData Text
                   | QuerySaveData
                   | QueryGame a
                   deriving (Eq)

instance FromJSON a => FromJSON (Query a) where
  parseJSON v = pSimple v <|> pSystem v <|> pUnknown v
    where
      pSimple = withObject "Query" $
        \x -> do
          cmd <- (x .: "cmd" :: Parser Text)
          case cmd of
            "keep-alive" -> pure QueryKeepAlive
            "kill" -> pure QueryKill
            _ -> fail $ "Unexpected text query"
      pSystem x = QuerySystem <$> parseJSON x
      pUnknown _ = pure QueryUnknown
instance FromJSON a => FromJSON (SystemQuery a) where
  parseJSON = withObject "SystemQuery" $
    \v -> do
      cmd <- (v .: "type" :: Parser Text)
      case cmd of
        "continue" -> pure QueryContinue
        "new-game" -> pure QueryCreateNewData
        "save" -> pure QuerySaveData
        "load" -> QueryLoadData <$> v .: "index"
        "list-all-save" -> pure QueryListAllSavedata
        "game" -> QueryGame <$> v .: "game"
        _ -> fail "Unexpected query type"




bytestringToQuery :: FromJSON a => ByteString -> Query a
bytestringToQuery bs =
  fromMaybe QueryUnexpected $ decodeStrict bs

isValidQuery :: Query a -> Bool
isValidQuery QueryUnknown = False
isValidQuery QueryUnexpected = False
isValidQuery _ = True


