{-# LANGUAGE OverloadedStrings,LambdaCase,RankNTypes #-}

module Game ( game, dataToSave ) where

import Relude.Extra.Newtype (un)

import Data.Default.Class
import Data.Aeson (ToJSON, FromJSON)
import qualified Data.Vector.Mutable as V
import Control.Lens (Lens', modifying, view, preview, use, assign, Traversal', mapMOf)
import qualified Log.State as Log
import Log.State (Logger)
import qualified Data.Text as T
import System.Random (StdGen, initStdGen)

import Response (Response(..), CommonJSONResponse(..))
import qualified Response as R
import qualified Query as Query
import qualified Game.State.GameState as S
import Game.Data.Class (toSaveForm)
import Game.State.GameState (_scene, _lowerGameState, _data, _count, _adventure, GameState, GameScene)
import Game.Data.DungeonMap (_cache, _pcix, getCellsAsDumpBuilder, putExtraInfo, DungeonMap)
import Game.Data.DungeonMapLike (DungeonMapLike, CharacterIx, getCharacterOthersFromIx, getAllChsFullSetBy)
import Game.DoEvent (Diff, phkpredToText, getSense, sense, doEventMove1Character, doDummyEventBeforeEnemyPhase, PartialTurnState(..), mkPartialTurnState, PhKPred, doAllEnemiesActions)
import Game.Data.Character (isPlayerCharacter)
import Game.Data.Dump.DumpBuilder (DumpBuilder, flatDumpBuilder)
import Game.Data.Adventure (Adventure, genDummyAdventure, _dungeonMap)
import Game.System (gameTop, WithLogger(..), GStateT, GameSystemST, GameST, internal, _state)
import qualified Game.Response as G
import qualified Game.Query as G
import Game.Query (Query(..), QueryBaseMenu(..), QueryAdventure(..), QueryPlayerCharacter(..), QueryDebug(..), QueryDumpType(..), QueryDumpControl(..))
import Game.Data.Direction (Direction)



jsonResponse :: Applicative f => a -> f (Response a)
jsonResponse = pure . ResponseJSON

type GameResponse = Response G.Response
type GameQuery = Query.Query Query

game :: GameQuery -> GameSystemST IO GameResponse
game = gameTop (lifted game0) dataToSave
  where
    lifted :: (Query -> GameST IO GameResponse) -> Query -> GameSystemST IO GameResponse
    lifted f q = internal _lowerGameState (ResponseJSONCommon CJFailure) (f q)

dataToSave :: GameST IO S.GameStateForSave
dataToSave = do
  s <- use _state
  liftIO $ toSaveForm s

dumpCurrentAdventureCacheData :: [QueryDumpControl] -> GameST IO ()
dumpCurrentAdventureCacheData _ = do
  s <- gets $ preview (_state . _scene . _adventure . _dungeonMap . _cache)
  case s of
    Just ss -> Log.debug (show ss :: Text)
    Nothing -> pure ()

dumpFloorMap :: [QueryDumpControl] -> GameST IO ()
dumpFloorMap plk = do
  dmM <- gets $ preview (_state . _scene . _adventure . _dungeonMap)
  case dmM of
    Nothing -> Log.warn ("No cuurent DungeonMap." :: Text)
    Just dm -> do
      fss <- liftIO $ getCellsAsDumpBuilder dm
      -- 追加情報を乗せる場合はここで DumpBuilder を上書きで修正する
      traverse_ (f dm fss) plk
      s <- liftIO $ flatDumpBuilder fss
      Log.debug ("\n" <> s)
  where
    f :: DungeonMap -> DumpBuilder -> QueryDumpControl -> GameST IO ()
    f dm vec qdc = do
      r <- liftIO $ putExtraInfo dm vec qdc
      case r of
        Nothing -> pure ()
        Just q -> Log.err ("Query failed. QueryDumpControl: " <> show q :: Text)


game0 :: Query -> GameST IO GameResponse
game0 QueryCountUp = do
  modifying (_state . _data . _count) (+1)
  i <- use (_state . _data . _count)
  jsonResponse $ G.ResponseCount i
game0 (QueryBaseMenu q) = do
  ev <- routeBaseMenu q
  case ev of
    Left c -> pure $ R.ResponseJSONCommon c
    Right r -> jsonResponse $ G.ResponseBaseMenu r
game0 (QueryAdventure q) = do
  r <- routeAdventure q
  pure $ R.ResponseJSONCommon r
game0 (QueryPlayerCharacter q) = do
  r <- routePlayerCharacter q
  pure $ R.ResponseJSONCommon CJSucceed
game0 (QueryDebug q) = do
  r <- routeDebug q
  pure $ R.ResponseJSONCommon r


routeDebug :: QueryDebug -> GameST IO CommonJSONResponse
routeDebug (DebugQueryDump t cs) = do
  case t of
    QDTCurrentAdventureCacheData -> do
      dumpCurrentAdventureCacheData cs
      pure CJSucceed
    QDTCurrentAdventureFloorMap -> do
      dumpFloorMap cs
      pure CJSucceed
    QDTUnknown unknownType -> do
      Log.err ("Unknown Dump Type: " <> unknownType)
      pure CJFailure


-- 排他2値を返すためだけの Either 使用。好ましくない
routeBaseMenu :: QueryBaseMenu -> GameST IO (Either CommonJSONResponse G.ResponseBaseMenu)
routeBaseMenu QueryListAdventures =
  pure $ Right $ G.ResponseListAdventures [G.ThinAdventureInfo 0 "wip00" "テスト00"]
routeBaseMenu (QueryInfoAdventure _advIx) =
  pure $ Right $ G.ResponseInfoAdventure $ G.AdventureInfo "wip00" "テスト00" False "テスト用ダンジョンです"
routeBaseMenu (QueryEnterAdventure _advIx) = do
  mA <- liftIO genDummyAdventure
  case mA of
    Nothing -> pure . Left $ CJFailure
    Just adv -> do
      assign (_state . _scene) (S.SceneAdventure adv)
      pure . Left $ CJSucceed


routeAdventure :: QueryAdventure -> GameST IO CommonJSONResponse
routeAdventure QueryAbortAdventure = do
  assign (_state . _scene) $ S.SceneBaseMenu
  Log.info ("Adventure retired." :: Text)
  pure CJSucceed


data ResponsePCK = ResponseLastTurnK
-- ここは基本的にターン進行処理になる
-- これが @ の取得できた K群 を返すといい
-- また ターン進行できない(不正な操作などで) 場合に進まないよう
--  1階層追加して ターンを進めてもよいかをcheckする処理が挟まる？
routePlayerCharacter :: QueryPlayerCharacter -> GameST IO ResponsePCK
routePlayerCharacter (QueryPCMove dir) = do
  underAdv_ $ eventMovePC1Step dir
  underAdv_ $ doAllEnemies
--  underAdv_ $ dummyEventBeforeEnemyPhase
  pure ResponseLastTurnK

type AdvST = GStateT Adventure

underAdv :: a -> AdvST IO a -> GameST IO a
underAdv = internal (_scene . _adventure)

underAdv_ :: AdvST IO () -> GameST IO ()
underAdv_ = underAdv ()


-- この下にあるような、ターン処理部分は TurnControl.hs などに纏めておきたい
applyDiffs :: [Diff DungeonMap] -> AdvST IO ()
applyDiffs diffs = do
  dm <- use (_state . _dungeonMap)
  newDM <- liftIO $ foldlM (\s diff -> diff s) dm diffs
  assign (_state . _dungeonMap) newDM

eventWithPlayerCharacter :: a -> (CharacterIx -> AdvST IO a) -> AdvST IO a
eventWithPlayerCharacter a f = do
  dm <- use (_state . _dungeonMap)
  case view (_cache . _pcix) dm of
    Nothing -> Log.err ("[eventWithPlayerCharacter] No index of @." :: Text) >> pure a
    Just cix -> f cix

eventWithPlayerCharacter_ :: (CharacterIx -> AdvST IO ()) -> AdvST IO ()
eventWithPlayerCharacter_ = eventWithPlayerCharacter ()

{-# DEPRECATED badStdGen "Unpreferred StdGen generation" #-}
badStdGen :: MonadIO m => m StdGen
badStdGen = initStdGen

eventMovePC1Step :: Direction -> AdvST IO ()
eventMovePC1Step dir = do
  eventWithPlayerCharacter_ $ \cix -> do
    dm <- use (_state . _dungeonMap)
    randomGen <- badStdGen
    (succeed, PartialTurnState _ _ newDM revPhks) <- liftIO $ runStateT (doEventMove1Character cix dir) (mkPartialTurnState randomGen dm)
    assign (_state . _dungeonMap) newDM
    case getCharacterOthersFromIx dm cix of
      Nothing -> Log.err ("[eventMovePC1Step] Missing character." :: Text)
      Just (c,p) -> do
        let preds = sense p cix (getSense c) (reverse revPhks)
        Log.info $ phkpredFormatter preds


phkpredFormatter :: [PhKPred] -> Text
phkpredFormatter preds =
  T.concat $ (addParens . phkpredToText) <$> preds
  where
    addParens :: Text -> Text
    addParens t = "[" <> t <> "]"


doAllEnemies :: AdvST IO ()
doAllEnemies = do
  dm <- use (_state . _dungeonMap)
  randomGen <- badStdGen
  (_, PartialTurnState _ _ newDM revPhks) <- liftIO $ runStateT doAllEnemiesActions (mkPartialTurnState randomGen dm)
  assign (_state . _dungeonMap) newDM
  eventWithPlayerCharacter_ $ \cix ->
    case getCharacterOthersFromIx dm cix of
      Nothing -> Log.err ("[doAllEnemies] Missing character." :: Text)
      Just (c,p) -> do
        let preds = sense p cix (getSense c) (reverse revPhks)
        Log.info $ phkpredFormatter preds



dummyEventBeforeEnemyPhase :: AdvST IO ()
dummyEventBeforeEnemyPhase = do
  dm <- use (_state . _dungeonMap)
  randomGen <- badStdGen
  (_, PartialTurnState _ _ newDM revPhks) <- liftIO $ runStateT doDummyEventBeforeEnemyPhase (mkPartialTurnState randomGen dm)
  assign (_state . _dungeonMap) newDM
  -- 通常のCは同陣営のCが何処に居るかを意思決定に使用しない
  --  進路上に他のCが居て邪魔になるなどは
  --  進みたい cell が進入可能かどうかで纏めて check する
  -- また PC の行動はここで決定してはいけないので、 PC以外の全てのCについての処理になる
  let cs = getAllChsFullSetBy dm (not . isPlayerCharacter)
  traverse_ (enemyTurn revPhks) cs
  where
    enemyTurn rPhks (c,p,ix) = do
      let preds = sense p ix (getSense c) (reverse rPhks)
      Log.info ("." :: Text)
      Log.info $ phkpredFormatter preds


