{-# LANGUAGE OverloadedStrings,TypeFamilies,LambdaCase, RankNTypes, FlexibleContexts #-}

module Game.State.GameState ( GameSystemState(..)
                            , GameSystemDefinition(..)
                            , GameSystemData(..)
                            , _savefiles
                            , GameSystemScene(..)
                            , _lowerGameState
                            , GameState(..)
                            , GameDefinition(..)
                            , GameData(..)
                            , _definition
                            , _data
                            , _scene
                            , GameScene(..)
                            , _adventure
                            , GameStateForSave
                            -- temp
                            , _count
                            ) where

import Data.Default.Class
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, (.:), (.:?), (.!=))
import qualified Data.Aeson as JSON
import Data.Aeson.Types (Parser)
import qualified Data.Aeson.Types as JSON
import Control.Lens (Lens', Getter, view, Traversal')

import Game.Definition
import Game.Version (VersionString(..))
import Game.Data.Class (HasSaveForm(..))
import Game.Data.Adventure (Adventure, AdventureForSave)


class LikeGameState a where
  type LikeDefinition a
  type LikeData a
  type LikeScene a
  _definition :: Lens' a (LikeDefinition a)
  _data :: Lens' a (LikeData a)
  _scene :: Lens' a (LikeScene a)

class (LikeGameState a, FromJSON (LikeDefinition a), FromJSON (LikeData a), FromJSON (LikeScene a)) => HasTrioConstructor a where
  trioConstructor :: LikeDefinition a -> LikeData a -> LikeScene a -> a
  helperStateFromJSON :: String -> JSON.Value -> JSON.Parser a
  helperStateFromJSON name =
    withObject name $
    \v -> trioConstructor
          <$> v .: "definition-version"
          <*> v .: "data"
          <*> v .: "scene"

helperStateToJSON :: (ToJSON df, ToJSON dt, ToJSON sc)
                  => Getter a df -> Getter a dt -> Getter a sc -> a -> JSON.Value
helperStateToJSON dfF dtF scF v =
  object [ "definition-version" .= view dfF v
         , "data" .= view dtF v
         , "scene" .= view scF v
         ]


data GameSystemState = GameSystemState GameSystemDefinition GameSystemData GameSystemScene
instance LikeGameState GameSystemState where
  type LikeDefinition GameSystemState = GameSystemDefinition
  type LikeData GameSystemState = GameSystemData
  type LikeScene GameSystemState = GameSystemScene
  _definition f (GameSystemState x b c) = (\a -> GameSystemState a b c) <$> f x
  _data f (GameSystemState a x c) = (\b -> GameSystemState a b c) <$> f x
  _scene f (GameSystemState a b x) = (\c -> GameSystemState a b c) <$> f x
instance HasTrioConstructor GameSystemState where
  trioConstructor = GameSystemState
instance Default GameSystemState where
  def = GameSystemState def def def
instance ToJSON GameSystemState where
  toJSON = helperStateToJSON _definition _data _scene
instance FromJSON GameSystemState where
  parseJSON = helperStateFromJSON "GameSystemState"

data GameSystemData = GameSystemData
  { gsdSavefiles :: [FilePath] -- Not for keep this field.
  } deriving (Eq)
instance Default GameSystemData where
  def = GameSystemData []
instance ToJSON GameSystemData where
  toJSON _ = object []
instance FromJSON GameSystemData where
  parseJSON = withObject "GameSystemData" $ (const $ pure def)
_savefiles :: Lens' GameSystemData [FilePath]
_savefiles f (GameSystemData x) = GameSystemData <$> f x


data GameSystemScene = SceneTitle
                     | SceneBeforeDataLoad
                     | SceneAfterDataLoad GameState
instance Default GameSystemScene where
  def = SceneTitle
instance ToJSON GameSystemScene where
  toJSON _ = JSON.Null
instance FromJSON GameSystemScene where
  parseJSON _ = pure SceneTitle

_lowerGameState :: Traversal' GameSystemState GameState
_lowerGameState = _scene . _lower
  where
    _lower f (SceneAfterDataLoad s) = SceneAfterDataLoad <$> f s
    _lower _ s = pure s



data GameState = GameState GameDefinition GameData GameScene
instance Default GameState where
  def = GameState def def def
instance LikeGameState GameState where
  type LikeDefinition GameState = GameDefinition
  type LikeData GameState = GameData
  type LikeScene GameState = GameScene
  _definition f (GameState x b c) = (\a -> GameState a b c) <$> f x
  _data f (GameState a x c) = (\b -> GameState a b c) <$> f x
  _scene f (GameState a b x) = (\c -> GameState a b c) <$> f x
instance HasSaveForm GameState where
  type LikeSaveForm GameState = GameStateForSave
  toSaveForm (GameState defs dts scene) = GameStateForSave defs dts <$> toSaveForm scene
  fromSaveForm (GameStateForSave defs dts scene) = GameState defs dts <$> fromSaveForm scene

data GameStateForSave = GameStateForSave GameDefinition GameData GameSceneForSave
instance LikeGameState GameStateForSave where
  type LikeDefinition GameStateForSave = GameDefinition
  type LikeData GameStateForSave = GameData
  type LikeScene GameStateForSave = GameSceneForSave
  _definition f (GameStateForSave x b c) = (\a -> GameStateForSave a b c) <$> f x
  _data f (GameStateForSave a x c) = (\b -> GameStateForSave a b c) <$> f x
  _scene f (GameStateForSave a b x) = (\c -> GameStateForSave a b c) <$> f x
instance HasTrioConstructor GameStateForSave where
  trioConstructor = GameStateForSave
instance ToJSON GameStateForSave where
  toJSON = helperStateToJSON _definition _data _scene
instance FromJSON GameStateForSave where
  parseJSON = helperStateFromJSON "GameStateForSave"


data GameData = GameData Int deriving (Eq) -- mock
instance Default GameData where
  def = GameData 0
instance ToJSON GameData where
  toJSON (GameData i) = object [ "count" .= i ]
instance FromJSON GameData where
  parseJSON = withObject "GameData" $
    \v -> GameData
          <$> v .:? "count" .!= 0

_count :: Lens' GameData Int
_count f (GameData x) = GameData <$> f x

data GameScene = SceneBaseMenu
               | SceneAdventure Adventure
instance Default GameScene where
  def = SceneBaseMenu
_adventure :: Traversal' GameScene Adventure
_adventure f SceneBaseMenu = pure SceneBaseMenu
_adventure f (SceneAdventure x) = SceneAdventure <$> f x

data GameSceneForSave = FSSceneBaseMenu
                      | FSSceneAdventure AdventureForSave
instance ToJSON GameSceneForSave where
  toJSON =
    \case
      FSSceneBaseMenu -> object [ "scene" .= ("menu" :: Text) ]
      FSSceneAdventure adv -> object [ "scene" .= ("adventure" :: Text)
                                     , "adventure" .= adv ]
instance FromJSON GameSceneForSave where
  parseJSON = withObject "GameSceneForSave" $
    \v -> do
      scene <- (v .: "scene" :: Parser Text)
      case scene of
        "menu" -> pure FSSceneBaseMenu
        "adventure" -> FSSceneAdventure <$> v .: "adventure"
        _ -> fail "Unknown scene type"
instance HasSaveForm GameScene where
  type LikeSaveForm GameScene = GameSceneForSave
  toSaveForm SceneBaseMenu = pure FSSceneBaseMenu
  toSaveForm (SceneAdventure adv) = FSSceneAdventure <$> toSaveForm adv
  fromSaveForm FSSceneBaseMenu = pure SceneBaseMenu
  fromSaveForm (FSSceneAdventure adv) = SceneAdventure <$> fromSaveForm adv


