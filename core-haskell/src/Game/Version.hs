module Game.Version ( VersionString(..)
                    ) where


newtype VersionString = VersionString Text deriving (Eq)


