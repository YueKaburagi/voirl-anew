{-# LANGUAGE OverloadedStrings,LambdaCase,RankNTypes #-}

module Game.System ( WithLogger(..)
                   , _state
                   , gameTop
                   , actionEndLifetime
                   , GStateT
                   , GameSystemST
                   , GameST
                   , internal
                   ) where

import Relude.Extra.Newtype (un)
import Relude.Extra.Bifunctor (firstF)

import Data.Default.Class
import Data.Aeson (ToJSON, FromJSON, eitherDecodeFileStrict', encodeFile)
import System.Directory (doesFileExist, doesDirectoryExist, createDirectoryIfMissing, listDirectory)
import Control.Lens (Lens', modifying, view, use, assign, Traversal', preview, mapMOf, firstOf)
import qualified Log.State as Log
import Log.State (HasLogger(..), Logger)

import Response (Response(..))
import qualified Response as R
import Query (Query, SystemQuery)
import qualified Query as Q
import qualified Game.State.GameState as S
import Game.Data.Class (fromSaveForm, toSaveForm)
import Game.State.GameState (_scene, _lowerGameState, _data, _savefiles)
import qualified System.FilePath.Posix as Path
-- import qualified System.FilePath.Windows as Path


data SystemMessage = NoSuchSave FilePath
                   | SystemError Text
                   deriving (Eq)

toPlainText :: SystemMessage -> Text
toPlainText (NoSuchSave path) = "Missing save data '" <> toText path <> "'."
toPlainText (SystemError txt) = "System Error: " <> txt


newtype ValidFilePath = ValidFilePath FilePath deriving (Eq, Show)
newtype ExistentFilePath = ExistentFilePath FilePath deriving (Eq, Show)

toValidSaveFilePath :: FilePath -> IO (Either SystemMessage ValidFilePath)
toValidSaveFilePath fileName = do
  let relBase = "../" <> "save"
  createDirectoryIfMissing True relBase -- Create parents too.
  saveDirExists <- liftIO $ doesDirectoryExist relBase
  if not saveDirExists
    then pure . Left . NoSuchSave $ fileName
    else do
    let relPath = relBase <> "/" <> fileName
    pure . Right . ValidFilePath $ relPath

toExistentSaveFilePath :: ValidFilePath -> IO (Either SystemMessage ExistentFilePath)
toExistentSaveFilePath vPath = do
  saveDataExists <- liftIO $ doesFileExist $ un vPath
  if not saveDataExists
    then pure . Left . NoSuchSave $ un vPath
    else pure . Right . ExistentFilePath $ un vPath


loadFromJSON :: FromJSON a => ExistentFilePath -> IO (Either SystemMessage a)
loadFromJSON path =
  firstF (SystemError . toText) $ eitherDecodeFileStrict' (un path)

saveToJSON :: ToJSON a => ValidFilePath -> a -> IO ()
saveToJSON path = encodeFile (un path)




data WithLogger a = WithLogger Logger a
instance HasLogger (WithLogger a) where
  _logger f (WithLogger x a) = (\l -> WithLogger l a) <$> f x
instance Functor WithLogger where
  fmap f (WithLogger l a) = WithLogger l $ f a


_state :: Lens' (WithLogger a) a
_state f (WithLogger l x) = WithLogger l <$> f x

type GStateT a = StateT (WithLogger a)

instantWithLogger :: Monad m => Logger -> GStateT s m a -> s -> m s
instantWithLogger l st s =
  view _state <$> execStateT st (WithLogger l s)

internal :: Monad m => Traversal' s a -> c -> GStateT a m c -> GStateT s m c
internal _under c lo = do
  (WithLogger logger s) <- get
  case preview _under s of
    Nothing -> pure c
    Just a -> do
      (v, WithLogger _ na) <- lift $ runStateT lo (WithLogger logger a)
      assign (_state . _under) na
      pure v


type GameSystemST = GStateT S.GameSystemState
type GameST = GStateT S.GameState


loadData :: Text -> GameSystemST IO (Response r)
loadData filenameWithExt = do
  let path = toString filenameWithExt
  eGSForL <- liftIO $ runExceptT $ toVP path >>= toEP >>= load
  case eGSForL of
    Left msg -> do
      Log.err $ toPlainText msg
      pure $ ResponseJSONCommon R.CJFailure
    Right gameStateForLoad -> do
      gameState <- liftIO $ fromSaveForm gameStateForLoad
      Log.info ("[" <> filenameWithExt <> "] is loaded.")
      assign (_state . _scene) $ S.SceneAfterDataLoad gameState
      pure $ ResponseJSONCommon R.CJSucceed
  where
    toVP p = ExceptT (toValidSaveFilePath p)
    toEP p = ExceptT (toExistentSaveFilePath p)
    load p = ExceptT (loadFromJSON p)


saveData :: ToJSON a => GameST IO a -> GameSystemST IO (Response r)
saveData dataToSave = do
  b1 <- saveGameSystemData "system"
  b2 <- internal _lowerGameState True $ saveGameData "game00" dataToSave
  if b1 && b2
    then pure $ ResponseJSONCommon R.CJSucceed
    else pure $ ResponseJSONCommon R.CJFailure
      
saveGameSystemData :: FilePath -> GameSystemST IO Bool
saveGameSystemData pathPrefix = do
  gameSystemData <- use _state
  let fileName = pathPrefix <> ".json"
  eVP <- liftIO $ toValidSaveFilePath fileName
  case eVP of
    Left msg -> do
      Log.err $ toPlainText msg
      pure False
    Right validPath -> do
      liftIO $ saveToJSON validPath gameSystemData
      Log.info ("[" <> fileName <> "] is saved.")
      pure True

saveGameData :: ToJSON a => FilePath -> GameST IO a -> GameST IO Bool
saveGameData pathPrefix dataToSave = do
  gameData <- dataToSave
  let fileName = pathPrefix <> ".json"
  eVP <- liftIO $ toValidSaveFilePath fileName
  case eVP of
    Left msg -> do
      Log.err $ toPlainText msg
      pure False
    Right validPath -> do
      liftIO $ saveToJSON validPath gameData
      Log.info ("[" <> fileName <> "] is saved.")
      pure True



createNewGameData :: GameSystemST IO (Response r)
createNewGameData = do
  assign (_state . _scene) (S.SceneAfterDataLoad def)
  Log.info ("Create new game data." :: Text)
  pure $ ResponseJSONCommon R.CJSucceed



listSavefiles :: GameSystemST IO ()
listSavefiles = do
  paths <- liftIO $ listDirectory "../save"
  let savefiles = filter isSavefileLike (Path.takeFileName <$> paths)
  if null savefiles
    then Log.info ("Empty save directory." :: Text)
    else assign (_state . _data . _savefiles) savefiles
  where
    isSavefileLike :: FilePath -> Bool
    isSavefileLike path = foldr (\f b -> b && f path) True [isPrefixOf "game", Path.isExtensionOf ".json"]

showKnownSavefiles :: GameSystemST IO (Response r)
showKnownSavefiles = do
  listSavefiles
  paths <- use $ _state . _data . _savefiles
  pure $ ResponseJSONCommon $ R.CJKnownSavefiles (toText <$> paths)


gameTop :: ToJSON sv => (a -> GameSystemST IO (Response r)) -> GameST IO sv -> Query a -> GameSystemST IO (Response r)
gameTop gameMain dataToSave =
  \case
    Q.QueryKeepAlive ->
      pure $ ResponseText "Received 'Keep alive'."
    Q.QueryKill ->
      pure $ ResponseText "MOCK"
    Q.QueryUnknown -> do
      let errstring = ("Unknown query received." :: Text)
      Log.warn errstring
      pure . ResponseJSONCommon . R.CJError $ errstring
    Q.QueryUnexpected -> do
      let errstring = ("Something wrong." :: Text)
      Log.warn errstring
      pure . ResponseJSONCommon . R.CJError $ errstring
    Q.QuerySystem q ->
      gameSystem gameMain dataToSave q

gameSystem :: ToJSON sv => (a -> GameSystemST IO (Response r)) -> GameST IO sv -> SystemQuery a -> GameSystemST IO (Response r)
gameSystem gameMain dataToSave =
  \case
    Q.QueryContinue ->
      pure $ ResponseText "Received 'Continue'."
    Q.QueryCreateNewData ->
      createNewGameData
    Q.QuerySaveData ->
      saveData dataToSave
    Q.QueryListAllSavedata ->
      showKnownSavefiles
    Q.QueryLoadData target ->
      loadData target
    Q.QueryGame q ->
      gameMain q


actionEndLifetime :: ToJSON sv => GameST IO sv -> GameSystemST IO ()
actionEndLifetime dataToSave = do
  Log.info ("Server lifetime ended." :: Text)
  _ <- saveData dataToSave
  pure ()
