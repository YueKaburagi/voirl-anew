{-# LANGUAGE TypeFamilies #-}

module Game.Data.Class ( HasSaveForm(..)
                       ) where

class HasSaveForm a where
  type LikeSaveForm a
  toSaveForm :: a -> IO (LikeSaveForm a)
  fromSaveForm :: LikeSaveForm a -> IO a
