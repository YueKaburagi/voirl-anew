module Game.Data.CharacterK ( CharacterK(..)
                            , CharacterKForCommon
                            , CharacterKForPlayer
                            , Faction(..)
                            , CharacterKLike(..)
                            ) where

import Game.Data.CharacterK.Types (CharacterK(..), CharacterKLike(..), Faction(..), CharacterKForCommon, CharacterKForPlayer)
import Game.Data.Pos (Pos(..))
import Game.Data.Direction (Direction)
import Control.Lens (view)

getCKEffect :: CharacterK -> [effect]
getCKEffect _ = []

getCKDirection :: CharacterK -> Maybe Direction
getCKDirection _ = Nothing


