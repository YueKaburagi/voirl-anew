module Game.Data.Dump.DumpBuilder ( ColorFlavor(..)
                                  , OptionalInfo(..)
                                  , FlavoredString(..)
                                  , _colorFlavor
                                  , _string
                                  , DumpBuilder
                                  , flatDumpBuilder
                                  ) where


import qualified Data.Vector.Mutable as V
import Control.Lens (Lens')


-- dump用、速度や賢さは無視、実装の楽さ優先
-- もう少し真面目にやるなら ansi-terminal を使ったほうがいいと思う

type DumpBuilder = V.IOVector FlavoredString

data ColorFlavor = CFNone
                 | CFBGDullBlue
                 deriving (Eq)

data OptionalInfo = OINone
                  | OIBegginingOfLine
                  deriving (Eq)

data FlavoredString = FlavoredString ColorFlavor OptionalInfo String

_colorFlavor :: Lens' FlavoredString ColorFlavor
_colorFlavor f (FlavoredString x b c) = (\a -> FlavoredString a b c) <$> f x
_string :: Lens' FlavoredString String
_string f (FlavoredString a b x) = (\c -> FlavoredString a b c) <$> f x

-- | For Dump.
flatDumpBuilder :: DumpBuilder -> IO String
flatDumpBuilder vec = snd <$> V.foldl' f (CFNone, "") vec
  where
    f (lastColor, s) (FlavoredString nextColor opt x) =
      let
        prefix0 = if not (null s) && opt == OIBegginingOfLine
                 then "\n"
                 else ""
        prefix1 = if lastColor == nextColor
                  then ""
                  else case nextColor of
                         CFNone -> "\ESC[0m"
                         CFBGDullBlue -> "\ESC[44m"
      in
        (nextColor, s <> prefix0 <> prefix1 <> x)
