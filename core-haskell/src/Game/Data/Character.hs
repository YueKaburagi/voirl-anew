{-# LANGUAGE OverloadedStrings #-}

module Game.Data.Character ( Character(..)
                           , isPlayerCharacter
                           , genDummyPlayerCharacter
                           , genDummyCharacter
                           ) where

import Data.Default.Class (def)
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, (.:))
import Data.Aeson.Types (Parser)
import Game.Data.KForPlayer (AdventureKForPlayer)
import Game.Data.KForCharacter (DungeonMapKForCharacter)


data Character = PlayerCharacter AdventureKForPlayer
               | Character DungeonMapKForCharacter
instance ToJSON Character where
  toJSON (PlayerCharacter k) = object [ "type" .= ("pc" :: Text)
                                      , "k" .= k ]
  toJSON (Character k) = object [ "type" .= ("c" :: Text)
                                , "k" .= k ]
instance FromJSON Character where
  parseJSON = withObject "Character" $
    \v -> do
      ty <- (v .: "type" :: Parser Text)
      case ty of
        "pc" -> PlayerCharacter <$> v .: "k"
        "c" -> Character <$> v .: "k"
        _ -> fail "unexpected character type"

isPlayerCharacter :: Character -> Bool
isPlayerCharacter (PlayerCharacter _) = True
isPlayerCharacter _ = False



genDummyPlayerCharacter :: IO Character
genDummyPlayerCharacter = pure $ PlayerCharacter def

genDummyCharacter :: IO Character
genDummyCharacter = pure $ Character def


-- Cog 系
data Cognizer = Cognizer

takeACog :: Character -> Cognizer
takeACog _ = Cognizer

cog :: Cognizer -> Phenomenon -> KUpdater
cog _ _ = KUpdater

kuExtractForPlayerCharacter :: KUpdater -> AdventureKForPlayer -> AdventureKForPlayer
kuExtractForPlayerCharacter _ = identity
kuExtractForCharacter :: KUpdater -> DungeonMapKForCharacter -> DungeonMapKForCharacter
kuExtractForCharacter _ = identity

-- Phenomenon はやや本質的でない名前だが、都合がいいのでこの名前とする
data Phenomenon = Phenomenon

data KUpdater = KUpdater
