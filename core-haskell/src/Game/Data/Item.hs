
module Game.Data.Item ( Item(..)
                      ) where

import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, (.:))


data Item = Item
instance ToJSON Item where
  toJSON _ = object []
instance FromJSON Item where
  parseJSON = withObject "Item" $
    \_ -> pure Item

