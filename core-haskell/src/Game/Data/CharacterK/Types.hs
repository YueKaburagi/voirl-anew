{-# LANGUAGE LambdaCase, OverloadedStrings #-}

module Game.Data.CharacterK.Types ( CharacterK(..)
                                  , CharacterKForCommon(..)
                                  , CharacterKForPlayer(..)
                                  , CharacterKLike(..)
                                  , Faction(..)
                                  ) where

import Game.Data.Pos (Pos)
import Control.Lens (Lens')
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, withText, (.:), (.:?))
import Data.Aeson.Types (Value(String))

-- ForP と ForCh は独立するかも
data CharacterK = CharacterKForPlayer CharacterKForCommon CharacterKForPlayer
                | CharacterKForCharacter CharacterKForCommon

-- 必ずしも内部で定義する必要のある要素ではないと思われる
-- newtype Faction = Faction Text とかもあり
data Faction = PlayerFaction
             | MarchantFaction
             | EnemyFaction
             deriving (Eq,Enum,Bounded)
instance ToJSON Faction where
  toJSON =
    \case
      PlayerFaction -> String "@"
      MarchantFaction -> String "m"
      EnemyFaction -> String "e"
instance FromJSON Faction where
  parseJSON = withText "Faction" $
    \case
      "@" -> pure PlayerFaction
      "m" -> pure MarchantFaction
      "e" -> pure EnemyFaction
      _ -> fail "Unexpected faction symbol."


class CharacterKLike a where
  _chkPos :: Lens' a Pos
  _chkFaction :: Lens' a Faction



instance CharacterKLike CharacterK where
  _chkPos = _interrnalCommon . _chkPos
  _chkFaction = _interrnalCommon . _chkFaction

_interrnalCommon :: Lens' CharacterK CharacterKForCommon
_interrnalCommon f (CharacterKForCharacter x) = CharacterKForCharacter <$> f x
_interrnalCommon f (CharacterKForPlayer x b) = (\a -> CharacterKForPlayer a b) <$> f x

instance ToJSON CharacterK where
  toJSON =
    \case
      CharacterKForPlayer c p ->
        object [ "common" .= c
               , "extra-player" .= p
               ]
      CharacterKForCharacter c ->
        object [ "common" .= c ]
instance FromJSON CharacterK where
  parseJSON = withObject "CharacterK" $
    \v -> do
      extraP <- v .:? "extra-player"
      case extraP of
        Nothing ->
          CharacterKForCharacter <$> v .: "common"
        Just p ->
          CharacterKForPlayer <$> v .: "common" <*> pure p


data CharacterKForCommon = ChKFCommon Pos Faction

instance CharacterKLike CharacterKForCommon where
  _chkPos f (ChKFCommon x b) = (\a -> ChKFCommon a b) <$> f x
  _chkFaction f (ChKFCommon a x) = (\b -> ChKFCommon a b) <$> f x
instance ToJSON CharacterKForCommon where
  toJSON (ChKFCommon pos faction) =
    object [ "pos" .= pos
           , "faction" .= faction
           ]
instance FromJSON CharacterKForCommon where
  parseJSON = withObject "CharacterKFroCommon" $
    \v -> ChKFCommon
          <$> v .: "pos"
          <*> v .: "faction"


data CharacterKForPlayer = ChKFPlayer

instance ToJSON CharacterKForPlayer where
  toJSON _ = object []
instance FromJSON CharacterKForPlayer where
  parseJSON = withObject "CharacterKForPlayer" $
    \_ -> pure ChKFPlayer
