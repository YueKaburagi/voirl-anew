module Game.Data.KForPlayer ( AdventureKForPlayer(..)
                            , DungeonMapKForPlayer(..) ) where

import Data.Default.Class
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, withText, (.:))


data AdventureKForPlayer = AdventureKForPlayer DungeonMapKForPlayer
instance Default AdventureKForPlayer where
  def = AdventureKForPlayer def
instance ToJSON AdventureKForPlayer where
  toJSON _ = object []
instance FromJSON AdventureKForPlayer where
  parseJSON = withObject "AdventureKForPlayer" $
    \_ -> pure $ AdventureKForPlayer def

data DungeonMapKForPlayer = DungeonMapKForPlayer
instance Default DungeonMapKForPlayer where
  def = DungeonMapKForPlayer
instance ToJSON DungeonMapKForPlayer where
  toJSON _ = object []
instance FromJSON DungeonMapKForPlayer where
  parseJSON = withObject "DungeonMapKForPlayer" $
    \_ -> pure DungeonMapKForPlayer

