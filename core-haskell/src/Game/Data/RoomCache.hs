module Game.Data.RoomCache ( RoomCache(..)
                           , _topLeft
                           , _bottomRight
                           , _width
                           , _height
                           , isInternalPos
                           ) where

import Control.Lens (Lens', view, Getter, to)
import Text.Show (showsPrec)

import Game.Data.Pos (Pos(..), _x, _y)

-- | RoomCache は 矩形内側 の 左上隅 と 右下隅 の座標を持つ
data RoomCache = RoomCache Pos Pos
               deriving (Eq)

_topLeft :: Lens' RoomCache Pos
_topLeft f (RoomCache x b) = (\a -> RoomCache a b) <$> f x
_bottomRight :: Lens' RoomCache Pos
_bottomRight f (RoomCache a x) = (\b -> RoomCache a b) <$> f x

_width :: Getter RoomCache Int
_width = to f
  where
    f (RoomCache a b) = view _x b - view _x a + 1
_height :: Getter RoomCache Int
_height = to f
  where
    f (RoomCache a b) = view _y b - view _y a + 1

instance Show RoomCache where
  showsPrec _ (RoomCache p0 p1) s = ("<" <> show p0 <> show p1 <> ">" ) ++ s

isInternalPos :: RoomCache -> Pos -> Bool
isInternalPos rCache p = view (_topLeft . _x) rCache <= view _x p
                         && view _x p <= view (_bottomRight . _x) rCache
                         && view (_topLeft . _y) rCache <= view _y p
                         && view _y p <= view (_bottomRight . _y) rCache

