{-# LANGUAGE OverloadedStrings,TypeFamilies #-}

module Game.Data.FloorMap ( FloorVec
                          , FloorMap(..)
                          , FreezedFloorMap(..)
                          , _width
                          , _cells
                          , reserveFloorMap
                          , isValidPosition
                          , flatPosition
                          , moundPosition
                          ) where

import Data.Default.Class
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, withText, (.:))
import qualified Data.Aeson as JSON
import Data.Aeson.Types (Parser)
import Control.Lens (Lens', view)
import Data.Vector.Mutable (IOVector)
import qualified Data.Vector.Mutable as V
import qualified Data.Vector as VI

import Game.Data.Class (HasSaveForm(..))
import Game.Data.Pos (Pos(..))
import Game.Data.Cell (Cell, genDummyCell)
import Game.Data.CellK.Types (CellK)
import Game.Data.KForPlayer (AdventureKForPlayer)
import Game.Data.KForCharacter (DungeonMapKForCharacter)


type FloorVec = IOVector Cell
data FloorMap = FloorMap Int FloorVec

_width :: Lens' FloorMap Int
_width f (FloorMap x b) = (\a -> FloorMap a b) <$> f x
_cells :: Lens' FloorMap FloorVec
_cells f (FloorMap a x) = (\b -> FloorMap a b) <$> f x

data FreezedFloorMap = FreezedFloorMap Int (VI.Vector Cell)
instance ToJSON FreezedFloorMap where
  toJSON (FreezedFloorMap w l) = object [ "width" .= w, "cells" .= l ]
instance FromJSON FreezedFloorMap where
  parseJSON = withObject "FreezedFloorMap" $
    \v -> FreezedFloorMap <$> v .: "width" <*> v .: "cells"
instance HasSaveForm FloorMap where
  type LikeSaveForm FloorMap = FreezedFloorMap
  toSaveForm (FloorMap w mvec) = FreezedFloorMap w <$> VI.freeze mvec
  fromSaveForm (FreezedFloorMap w vec) = FloorMap w <$> VI.thaw vec


reserveFloorMap :: Int -> Int -> IO (Maybe FloorMap)
reserveFloorMap 0 _ = pure Nothing
reserveFloorMap _ 0 = pure Nothing
reserveFloorMap width height = do
  cell <- genDummyCell
  Just . FloorMap width <$> V.replicate (width * height) cell

isValidPosition :: FloorMap -> Pos -> Bool
isValidPosition (FloorMap width vec) (Pos (x,y)) =
  0 < x && x < width && 0 < y && y < height
  where
    height = V.length vec `div` width

flatPosition :: FloorMap -> Pos -> Int
flatPosition (FloorMap width _) (Pos (x,y)) =
  y * width + x

moundPosition :: FloorMap -> Int -> Pos
moundPosition (FloorMap width _) n =
  Pos (n `mod` width, n `div` width)




type FloorVecK = IOVector CellK
data FloorMapK = FloorMapK Int Int FloorVecK
data KnownAtlasRectangle = KARectangle !Int !Int !Int !Int

expandKnownAtlas :: KnownAtlasRectangle -> KnownAtlasRectangle -> KnownAtlasRectangle
expandKnownAtlas (KARectangle x0_a y0_a x1_a y1_a) (KARectangle x0_b y0_b x1_b y1_b) =
  KARectangle
  (x0_a `max` x0_b)
  (y0_a `max` y0_b)
  (x1_a `max` x1_b)
  (y1_a `max` y1_b)

reserveFloorMapK :: FloorMap -> IO FloorMapK
reserveFloorMapK (FloorMap w vec) = FloorMapK offset width <$> V.replicate (width * height) def
  where
    width = 2 + w
    height = 2 + (V.length vec `div` w)
    offset = 1

