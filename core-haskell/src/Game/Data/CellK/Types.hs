module Game.Data.CellK.Types ( CellK(..)
                             ) where

import Data.Default.Class
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, withText, (.:))


data CellK = CellK
instance Default CellK where
  def = CellK
instance ToJSON CellK where
  toJSON _ = object []
instance FromJSON CellK where
  parseJSON = withObject "CellK" $
    \_ -> pure CellK


