{-# LANGUAGE OverloadedStrings,LambdaCase #-}

module Game.Data.Cell ( Cell(..)
                      , CellType(..)
                      , cellType2SingletonString
                      , CellTrait(..)
                      , CellFlavor(..)
                      , genDummyCell
                      , _cellType
                      , _traits
                      , _flavor
                      , hasTrait
                      ) where

import Data.Default.Class
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, withText, (.:), (.:?), (.!=))
import qualified Data.Aeson as JSON
import qualified Data.Set as Set
import Control.Lens (Lens', view)


data CellType = CellTypeFloor
              | CellTypeWall
              deriving (Eq)
instance Default CellType where
  def = CellTypeWall
instance ToJSON CellType where
  toJSON = JSON.String . toText . cellType2SingletonString
instance FromJSON CellType where
  parseJSON = withText "CellType" $
    \case
      "." -> pure CellTypeFloor
      "#" -> pure CellTypeWall
      _ -> fail "Unexpected cell type."

cellType2SingletonString :: CellType -> String
cellType2SingletonString CellTypeFloor = "."
cellType2SingletonString CellTypeWall = "#"

data CellTrait = RoomSeed
               deriving (Eq, Ord)
instance ToJSON CellTrait where
  toJSON =
    \case
      RoomSeed -> JSON.String ","
instance FromJSON CellTrait where
  parseJSON = withText "CellTrait" $
    \case
      "," -> pure RoomSeed
      _ -> fail "Unexpected cell trait."

data CellFlavor = CellFlavor

instance ToJSON CellFlavor where
  toJSON _ = object []
instance FromJSON CellFlavor where
  parseJSON = withObject "CellFlavor" $
    \_ -> pure CellFlavor

data Cell = Cell CellType (Set CellTrait) CellFlavor

_cellType :: Lens' Cell CellType
_cellType f (Cell x b c) = (\a -> Cell a b c) <$> f x
_traits :: Lens' Cell (Set CellTrait)
_traits f (Cell a x c) = (\b -> Cell a b c) <$> f x
_flavor :: Lens' Cell CellFlavor
_flavor f (Cell a b x) = (\c -> Cell a b c) <$> f x

hasTrait :: CellTrait -> Cell -> Bool
hasTrait t c = Set.member t $ view _traits c

instance ToJSON Cell where
  toJSON v = object $ [ "type" .= view _cellType v
                      , "flavor" .= view _flavor v
                      ] <> traits
    where
      traits =
        if null (view _traits v)
        then []
        else [ "traits" .= view _traits v ]
instance FromJSON Cell where
  parseJSON = withObject "Cell" $
    \v -> Cell
          <$> v .: "type"
          <*> v .:? "traits" .!= mempty
          <*> v .: "flavor"


genDummyCell :: IO Cell
genDummyCell = pure $ Cell def mempty CellFlavor
