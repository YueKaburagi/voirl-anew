{-# LANGUAGE OverloadedStrings #-}

module Game.Data.Pos.Types ( Pos(..)
                           , _x, _y
                           ) where

import Prelude hiding (show)
import Text.Show (Show(..))
import Control.Lens (Lens', _1, _2)
import Data.Inversity (Inversity(..))
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, (.:))

newtype Pos = Pos (Int,Int)
            deriving (Eq)
instance Semigroup Pos where
  (Pos (x0,y0)) <> (Pos (x1,y1)) = Pos (x0+x1, y0+y1)
instance Monoid Pos where
  mempty = Pos (0,0)
instance Inversity Pos where
  invert (Pos (x,y)) = Pos (negate x, negate y)
instance Show Pos where
  show (Pos (x,y)) = "(" <> show x <> ", " <> show y <> ")"

instance ToJSON Pos where
  toJSON (Pos (x,y)) = object [ "x" .= x
                              , "y" .= y
                              ]
instance FromJSON Pos where
  parseJSON = withObject "Pos" $
    \v -> do
      x <- v .: "x"
      y <- v .: "y"
      pure $ Pos (x,y)

_un :: Lens' Pos (Int, Int)
_un f (Pos x) = Pos <$> f x

_x :: Lens' Pos Int
_x = _un . _1

_y :: Lens' Pos Int
_y = _un . _2
