module Game.Data.RoomCacheGen ( genRoomCache ) where

import Relude.Extra.Foldable1 (minimum1, maximum1)

import Control.Lens (view)

import Game.Data.DungeonMapLike (DungeonMapLike(..), getCell)
import Game.Data.Pos (Pos(..), diffTopLeftSquare, diffTopRightSquare, diffBottomLeftSquare, diffBottomRightSquare)
import Game.Data.FloorMap (isValidPosition)
import Game.Data.Cell (Cell, _cellType)
import qualified Game.Data.Cell as C
import Game.Data.RoomCache (RoomCache(..))



-- | 仮部屋
type TempRect = (Pos, Pos)


-- | 指定した座標を種として RoomCache の生成を試みる
genRoomCache :: DungeonMapLike dm => dm -> Pos -> IO (Maybe RoomCache)
genRoomCache dm seedPos = do
  p4 <- canGrowthRoomCache dm seedPos
  case box2x2toRect p4 of
    Nothing -> pure Nothing
    Just rect -> do
      (p0, p1) <- genRoomCache0 dm rect
      if isValidRoomCacheSize (p0,p1)
        then pure . Just $ RoomCache p0 p1
        else pure Nothing

-- | 仮部屋を、左、上、右、下の順で1列ずつ広げようとする。を繰り返し、広げきった仮部屋を返す
genRoomCache0 :: DungeonMapLike dm => dm -> TempRect -> IO TempRect
genRoomCache0 dm rect = do
  (n,r) <- foldlM continue (0,rect) [expandLeft dm, expandTop dm, expandRight dm, expandBottom dm]
  if n > 0
    then genRoomCache0 dm r
    else pure r
  where
    continue :: (Int, TempRect) -> (TempRect -> IO (Maybe TempRect)) -> IO (Int, TempRect)
    continue (n,r) f = do
      mR <- f r
      case mR of
        Just newR -> pure (n+1,newR)
        Nothing -> pure (n,r)

-- | 2x2の4マスを仮部屋に変形する
box2x2toRect :: [Pos] -> Maybe TempRect
box2x2toRect [Pos (x0,y0), Pos (x1,y1), Pos (x2,y2), Pos (x3,y3)] =
  Just $ (Pos (minimum1 xs, minimum1 ys), Pos (maximum1 xs, maximum1 ys))
  where
    xs = x0 :| [x1, x2, x3]
    ys = y0 :| [y1, y2, y3]
box2x2toRect _ = Nothing



lineLeft :: TempRect -> [Pos]
lineLeft (Pos (x0,y0), Pos (_,y1)) = [ Pos (x0 -1, y) | y <- [y0..y1] ]

lineRight :: TempRect -> [Pos]
lineRight (Pos (_,y0), Pos (x1,y1)) = [ Pos (x1 +1, y) | y <- [y0..y1] ]

lineTop :: TempRect -> [Pos]
lineTop (Pos (x0,y0), Pos (x1,_)) = [ Pos (x, y0 -1) | x <- [x0..x1] ]

lineBottom :: TempRect -> [Pos]
lineBottom (Pos (x0,_), Pos (x1,y1)) = [ Pos (x, y1 +1) | x <- [x0..x1] ]


expandLeft :: DungeonMapLike dm => dm -> TempRect -> IO (Maybe TempRect)
expandLeft = tryExpandTempRect (Pos (-1,0)) mempty lineLeft
expandRight :: DungeonMapLike dm => dm -> TempRect -> IO (Maybe TempRect)
expandRight = tryExpandTempRect mempty (Pos (1,0)) lineRight
expandTop :: DungeonMapLike dm => dm -> TempRect -> IO (Maybe TempRect)
expandTop = tryExpandTempRect (Pos (0,-1)) mempty lineTop
expandBottom :: DungeonMapLike dm => dm -> TempRect -> IO (Maybe TempRect)
expandBottom = tryExpandTempRect mempty (Pos (0,1)) lineBottom

-- | `expandTempRect` の前に bound check する版
tryExpandTempRect :: DungeonMapLike dm
                  => Pos -> Pos -> (TempRect -> [Pos])
                  -> dm -> TempRect -> IO (Maybe TempRect)
tryExpandTempRect diffTL diffBR genOuterLine dm rect =
  if passBound diffTL diffBR dm rect
  then expandTempRect diffTL diffBR genOuterLine dm rect
  else pure Nothing

-- | 広げようとしている方向が、データ構造的に許されている方向かを見る
passBound :: DungeonMapLike dm => Pos -> Pos -> dm -> TempRect -> Bool
passBound diffTL diffBR dm (p0, p1) =
  let
    b1 = isValidPosition (view _floorMap dm) (p0 <> diffTL)
    b2 = isValidPosition (view _floorMap dm) (p1 <> diffBR)
  in
    b1 && b2

-- | 指定した方向の外周が部屋に相応わしい Cell かどうかを見て、全て相応しい Cell なら矩形を広げる
--   そうでなければ失敗する
--   No bound check.
expandTempRect :: DungeonMapLike dm
               => Pos -> Pos -> (TempRect -> [Pos])
               -> dm -> TempRect -> IO (Maybe TempRect)
expandTempRect diffTL diffBR genOuterLine dm rect@(p0, p1) = do
  b <- allM (fmap isRoomCellLike . getCell dm) $ genOuterLine rect
  if b
    then pure . Just $ (p0 <> diffTL, p1 <> diffBR)
    else pure Nothing

-- | そのセルが部屋要素として相応わしいかを判定する
isRoomCellLike :: Cell -> Bool
isRoomCellLike c = C.CellTypeFloor == view _cellType c

-- | 2x2に成長できそうならその4マスを返す。そうでなければ mempty を返す
canGrowthRoomCache :: DungeonMapLike dm => dm -> Pos -> IO [Pos]
canGrowthRoomCache dm p =
  takeOneOrEmpty <$> filterM (fmap isPassedAll . canGrowth0) fourSquares
  where
    fourSquares = [ diffTopLeftSquare p, diffTopRightSquare p
                  , diffBottomLeftSquare p, diffBottomRightSquare p
                  ]
    canGrowth0 :: [Pos] -> IO [Pos]
    canGrowth0 = filterM (fmap isRoomCellLike . getCell dm)
    isPassedAll :: [Pos] -> Bool
    isPassedAll ps = 3 == (length ps)
    takeOneOrEmpty :: [[Pos]] -> [Pos]
    takeOneOrEmpty (x:_) = (p:x)
    takeOneOrEmpty _ = []


rectSize :: TempRect -> Int
rectSize (Pos (x0,y0), Pos (x1,y1)) = (abs (x0 - x1) + 1) * (abs (y0 - y1) + 1)

-- 3x4=12,  3x5=15, 4x4=16 なので 15 くらいからが最小の部屋扱いする
isValidRoomCacheSize :: TempRect -> Bool
isValidRoomCacheSize rect = 15 <= rectSize rect




