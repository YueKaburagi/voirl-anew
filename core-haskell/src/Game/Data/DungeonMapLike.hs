{-# LANGUAGE OverloadedStrings, LambdaCase #-}

module Game.Data.DungeonMapLike ( DungeonMapLike(..)
                                , FlatPos(..)
                                , CharacterIx(..)
                                , PosCIxMap
                                , CIxPosMap
                                , CharacterMap
                                , ItemMap
                                , FlatCharacterMap
                                , CharacterFullSet
                                , toFlatCharacterMap
                                , fromFlatCharacterMap
                                , to2dimPos
                                , to1dimPos
                                , getCell
                                , modifyCell
                                , getCharacterFromIx
                                , getCharacterFromPos
                                , getCharacterIxFromPos
                                , getCharacterPosFromIx
                                , getCharacterOthersFromPos
                                , getCharacterOthersFromIx
                                , getChAndPosFromIx
                                , getAllChsAndPosBy
                                , getAllChsFullSetBy
                                , getAllCharactersBy
                                , mkDiffUpdateAllCharactersWithPos
                                , updateCharacter
                                , alterCharacter
                                , mkDiffModifyCharacter
                                , modifyCharacterWith
                                , modify2CharacterWith
                                , modify2CharacterWithFullSet
                                , modify2CharacterWithFullSetM
                                , setCharacter
                                , mkDiffRemoveCharacterIf
                                , lookupPlayerCharacterIx
                                , getItem
                                , updateItem
                                , alterItem
                                -- DoEvent Support
                                , mkDiffReplaceCharacter
                                ) where

import Relude.Extra.Newtype (un, wrap, under)

import Data.Default.Class (Default(..))
import Control.Lens (Lens', view, over, use, modifying)
import qualified Data.Vector.Mutable as V
import qualified Data.IntMap.Strict as IntMap
import qualified Data.Map.Strict as Map
import Data.Aeson (ToJSON, toJSON, FromJSON, parseJSON, ToJSONKey, toJSONKey, FromJSONKey, fromJSONKey)

import Data.Inversity (invert)
import Game.Data.Pos (Pos, distance)
import Game.Data.FloorMap (FloorMap, _cells, flatPosition, moundPosition, isValidPosition)
import Game.Data.Cell (Cell, _cellType, CellType(..))
import Game.Data.Character (Character, isPlayerCharacter)
import Game.Data.Item (Item)

import Game.Data.Direction (Direction, dirToPos)

newtype FlatPos = FlatPos Int deriving (Eq,Ord)
newtype CharacterIx = CharacterIx Int deriving (Eq,Ord)
type CharacterMap = IntMap Character -- Map CharacterIx Character
type PosCIxMap = IntMap CharacterIx
type CIxPosMap = IntMap FlatPos
type ItemMap = IntMap Item

-- Type bindings of `un`.
-- Internal use only.
unFlatPos :: FlatPos -> Int
unFlatPos = un
unCIx :: CharacterIx -> Int
unCIx = un

-- For SaveForm
type FlatCharacterMap = IntMap Character

class DungeonMapLike a where
  _floorMap :: Lens' a FloorMap
  _characterMap :: Lens' a CharacterMap
  _posCIxMap :: Lens' a PosCIxMap
  _cixPosMap :: Lens' a CIxPosMap
  _itemMap :: Lens' a ItemMap

-- Transform for SaveForm
toFlatCharacterMap :: DungeonMapLike dm => dm -> FlatCharacterMap
toFlatCharacterMap dm =
  IntMap.foldlWithKey f mempty $ view _posCIxMap dm
  where
    f :: FlatCharacterMap -> Int -> CharacterIx -> FlatCharacterMap
    f fcm n cix =
      case IntMap.lookup (unCIx cix) masterMap of
        Nothing -> fcm
        Just c -> IntMap.insert n c fcm
    masterMap :: CharacterMap
    masterMap = view _characterMap dm

-- Transform for SaveForm
type ThreeCharaceterMapMix = (PosCIxMap, CIxPosMap, CharacterMap)
fromFlatCharacterMap :: FlatCharacterMap -> (PosCIxMap, CIxPosMap, CharacterMap)
fromFlatCharacterMap fcm =
  IntMap.foldlWithKey f (mempty, mempty, mempty) fcm
  where
    f :: ThreeCharaceterMapMix -> Int -> Character -> ThreeCharaceterMapMix
    f (pcm, cpm, cm) n c =
      let
        nextIx = CharacterIx $ getNextIx cpm
      in
        ( IntMap.insert n nextIx pcm
        , IntMap.insert (unCIx nextIx) (FlatPos n) cpm
        , IntMap.insert (unCIx nextIx) c cm
        )
    getNextIx cpm = maybe 0 (+1) . fmap fst $ IntMap.lookupMax cpm


to2dimPos :: DungeonMapLike dm => dm -> FlatPos -> Pos
to2dimPos dm = moundPosition (view _floorMap dm) . un

to1dimPos :: DungeonMapLike dm => dm -> Pos -> FlatPos
to1dimPos dm = wrap . flatPosition (view _floorMap dm)



getCell :: DungeonMapLike dm => dm -> Pos -> IO Cell
getCell dm p =
  V.read (view (_floorMap . _cells) dm) $ un $ to1dimPos dm p

-- | CAUTION: Mutable function.
modifyCell :: DungeonMapLike dm => dm -> Pos -> (Cell -> IO Cell) -> IO ()
modifyCell dm p f =
  V.modifyM (view (_floorMap . _cells) dm) f $ un $ to1dimPos dm p


raiseF :: Applicative f => (a -> f (Maybe a)) -> Maybe a -> f (Maybe a)
raiseF _ Nothing = pure Nothing
raiseF f (Just x) = f x


type CharacterFullSet = (Character, Pos, CharacterIx)

getCharacterFromIx :: DungeonMapLike dm => dm -> CharacterIx -> Maybe Character
getCharacterFromIx dm cix =
  IntMap.lookup (un cix) $ view _characterMap dm

getCharacterFromPos :: DungeonMapLike dm => dm -> Pos -> Maybe Character
getCharacterFromPos dm p =
  getCharacterFromIx dm =<< getCharacterIxFromPos dm p

getCharacterIxFromPos :: DungeonMapLike dm => dm -> Pos -> Maybe CharacterIx
getCharacterIxFromPos dm p =
  IntMap.lookup (un (to1dimPos dm p)) $ view _posCIxMap dm

getCharacterPosFromIx :: DungeonMapLike dm => dm -> CharacterIx -> Maybe Pos
getCharacterPosFromIx dm cix =
  fmap (to2dimPos dm) . IntMap.lookup (un cix) $ view _cixPosMap dm

getCharacterOthersFromPos :: DungeonMapLike dm => dm -> Pos -> Maybe (Character, CharacterIx)
getCharacterOthersFromPos dm p = do
  cix <- getCharacterIxFromPos dm p
  c <- getCharacterFromIx dm cix
  return (c, cix)

getCharacterOthersFromIx :: DungeonMapLike dm => dm -> CharacterIx -> Maybe (Character, Pos)
getCharacterOthersFromIx dm cix =
  (,) <$> getCharacterFromIx dm cix <*> getCharacterPosFromIx dm cix

{-# DEPRECATED getChAndPosFromIx "Use getCharacterOthersFromIx instead" #-}
getChAndPosFromIx :: DungeonMapLike dm => dm -> CharacterIx -> Maybe (Character, Pos)
getChAndPosFromIx = getCharacterOthersFromIx


getAllChsFullSetBy :: DungeonMapLike dm => dm -> (Character -> Bool) -> [CharacterFullSet]
getAllChsFullSetBy dm f =
  IntMap.foldlWithKey g [] $ view _characterMap dm
  where
    g s cix c | not (f c) = s
              | otherwise =
                case IntMap.lookup (un cix) $ view _cixPosMap dm of
                  Nothing -> s
                  Just p -> (c, to2dimPos dm p, CharacterIx cix):s

getAllChsAndPosBy :: DungeonMapLike dm => dm -> (Character -> Bool) -> [(Character, Pos)]
getAllChsAndPosBy dm f =
  IntMap.foldlWithKey g [] $ toFlatCharacterMap dm
  where
    g s p c | f c = (c, to2dimPos dm (FlatPos p)):s
            | otherwise = s

getAllCharactersBy :: DungeonMapLike dm => dm -> (Character -> Bool) -> [Character]
getAllCharactersBy dm f =
  IntMap.foldl g [] $ view _characterMap dm
  where
    g s c | f c = c:s
          | otherwise = s

mkDiffUpdateAllCharactersWithPos :: DungeonMapLike dm => (Character -> Pos -> Character) -> dm -> dm
mkDiffUpdateAllCharactersWithPos f dm =
  over _characterMap (IntMap.mapWithKey g) dm
  where
    g cix c =
      case IntMap.lookup cix (view _cixPosMap dm) of
        Nothing -> c
        Just p -> f c (to2dimPos dm p)


modify2CharacterWithFullSetM :: (DungeonMapLike dm, Monad m)
                             => (CharacterFullSet -> CharacterFullSet -> m (Character, Character, a))
                             -> CharacterIx -> CharacterIx -> dm -> m (Maybe (dm ,a))
modify2CharacterWithFullSetM fM cixL cixR dm =
  runMaybeT $ do
  cfullL <- MaybeT . pure $ getFullSet cixL
  cfullR <- MaybeT . pure $ getFullSet cixR
  lift $ ret cfullL cfullR
  where
    append c (a,b) = (a,b,c)
    getFullSet cix = append cix <$> getCharacterOthersFromIx dm cix
    upd cix = IntMap.insert (unCIx cix)
    ret cl cr = do
      (newCL, newCR, a) <- fM cl cr
      return (over _characterMap (upd cixR newCR . upd cixL newCL) dm, a)

modify2CharacterWithFullSet :: DungeonMapLike dm
                            => ((CharacterFullSet, CharacterFullSet) -> (Character, Character, a))
                            -> CharacterIx -> CharacterIx -> dm -> Maybe (dm, a)
modify2CharacterWithFullSet f cixL cixR dm =
  ret <$> getFullSet cixL <*> getFullSet cixR
  where
    append c (a,b) = (a,b,c)
    getFullSet cix = append cix <$> getCharacterOthersFromIx dm cix
    ret cl cr =
      let
        (newCL, newCR, a) = f (cl, cr)
        updL = IntMap.insert (un cixL) newCL
        updR = IntMap.insert (un cixR) newCR
      in
        (over _characterMap (updR . updL) dm, a)

modify2CharacterWith :: DungeonMapLike dm
                     => ((Character, Character) -> (Character, Character, a))
                     -> CharacterIx -> CharacterIx -> dm -> Maybe (dm, a)
modify2CharacterWith f cixL cixR dm =
  ret <$> IntMap.lookup (un cixL) cm <*> IntMap.lookup (un cixR) cm
  where
    cm = view _characterMap dm
    ret cl cr =
      let
        (newCL, newCR, a) = f (cl, cr)
        updL = IntMap.insert (un cixL) newCL
        updR = IntMap.insert (un cixR) newCR
      in
        (over _characterMap (updR . updL) dm, a)

modifyCharacterWith :: DungeonMapLike dm
                    => (Character -> (Character, a))
                    -> CharacterIx -> dm -> Maybe (dm, a)
modifyCharacterWith f cix dm =
  ret <$> IntMap.lookup (un cix) (view _characterMap dm)
  where
    ret c =
      let
        (newC, a) = f c
      in
        (over _characterMap (IntMap.insert (un cix) newC) dm, a)


mkDiffModifyCharacter :: DungeonMapLike dm => (Character -> Character) -> CharacterIx -> dm -> dm
mkDiffModifyCharacter f cix dm =
  over _characterMap (IntMap.alter (fmap f) (un cix)) dm

{-# DEPRECATED updateCharacter "Use mkDiffModifyCharacter instead" #-}
updateCharacter :: DungeonMapLike dm => dm -> CharacterIx -> (Character -> IO (Maybe Character)) -> IO dm
updateCharacter dm cix f = alterCharacter dm (un cix) (raiseF f)

{-# DEPRECATED alterCharacter "Use mkDiffModifyCharacter instead" #-}
alterCharacter :: DungeonMapLike dm => dm -> CharacterIx -> (Maybe Character -> IO (Maybe Character)) -> IO dm
alterCharacter dm cix f =
  dm & _characterMap (IntMap.alterF f (un cix))

-- | 指定した character を pos に追加し、その character に割り当てられた CIx を併せて返す
--   cix は 現存している cix の最大値より 1 大きい値
--   character の数が大き過ぎない前提の設計
setCharacter :: DungeonMapLike dm => Pos -> Character -> dm -> (dm, CharacterIx)
setCharacter p c dm =
  ( setCP . addCh $ dm
  , CharacterIx nextIx
  )
  where
    nextIx = maybe 0 (+1) . fmap fst $ IntMap.lookupMax (view _characterMap dm)
    addCh = over _characterMap (IntMap.insert nextIx c)
    setCP = setPosAndCIx (to1dimPos dm p) (wrap nextIx)

lookupPlayerCharacterIx :: DungeonMapLike dm => dm -> Maybe CharacterIx
lookupPlayerCharacterIx dm =
  IntMap.foldlWithKey f Nothing $ view _characterMap dm
  where
    f s cix c | isJust s = s
              | isPlayerCharacter c = Just $ CharacterIx cix
              | otherwise = Nothing

removeCharacter :: DungeonMapLike dm => CharacterIx -> dm -> dm
removeCharacter cix dm =
  rmFromMaster . rmCIxFromCIxPos cix . freePastPos $ dm
  where
    rmFromMaster = over _characterMap (IntMap.delete $ unCIx cix)
    companionPos = IntMap.lookup (unCIx cix) $ view _cixPosMap dm
    freePastPos = maybe id rmPosFromPosCIx companionPos

-- | 死亡条件チェックとか？
mkDiffRemoveCharacterIf :: DungeonMapLike dm => (Character -> Bool) -> CharacterIx -> dm -> Maybe dm
mkDiffRemoveCharacterIf f cix dm =
  case IntMap.lookup (un cix) $ view _characterMap dm of
    Nothing -> Nothing
    Just c | f c -> Just $ removeCharacter cix dm
           | otherwise -> Nothing
  

-- | CharacterMap を更新せず、 (Pos, CIx) の組を上書き追加する
--   これは Orphan な Pos -> CIx や CIx -> Pox の原因となる
--   Internal use only.
setPosAndCIx :: DungeonMapLike dm => FlatPos -> CharacterIx -> dm -> dm
setPosAndCIx p cix = setPosCIx . setCIxPos
  where
    setCIxPos = over _cixPosMap $ IntMap.insert (un cix) p
    setPosCIx = over _posCIxMap $ IntMap.insert (un p) cix

-- | CharacterMap を更新せず、Pos -> CIx を削除する
--   これは Orphan な CIx -> Pos の原因となる
--   Internal use only.
rmPosFromPosCIx :: DungeonMapLike dm => FlatPos -> dm -> dm
rmPosFromPosCIx p = over _posCIxMap $ IntMap.delete (un p)

-- | CharacterMap を更新せず、CIx -> Pos を削除する
--   これは Orphan な Pos -> CIx の原因となる
--   Internal use only.
rmCIxFromCIxPos :: DungeonMapLike dm => CharacterIx -> dm -> dm
rmCIxFromCIxPos cix = over _cixPosMap $ IntMap.delete (un cix)

-- | CharacterMap を更新せず、 (Pos,CIx) の組を削除する
--   これは Orhpan な CIx -> Character の原因となる
--   Internal use only.
rmPosAndCIx :: DungeonMapLike dm => FlatPos -> CharacterIx -> dm -> dm
rmPosAndCIx p cix = rmCIxFromCIxPos cix . rmPosFromPosCIx p

-- | 指定した CIx を Pos に配置する。
--   直前に Pos に存在していた CIx があれば その CIx -> Character は消える
--   直前に CIx が存在していた Pos を空位にする
mkDiffReplaceCharacter :: DungeonMapLike dm => Pos -> CharacterIx -> dm -> dm
mkDiffReplaceCharacter newPos cix dm =
  freePastPos . setCP . freeTargetPos $ dm
  where
    setCP = setPosAndCIx (to1dimPos dm newPos) cix
    freeTargetPos = maybe id removeCharacter previx
    previx = getCharacterIxFromPos dm newPos
    freePastPos = maybe id rmPosFromPosCIx prevpos
    prevpos = IntMap.lookup (un cix) $ view _cixPosMap dm



getItem :: DungeonMapLike dm => dm -> Pos -> Maybe Item
getItem dm p =
  IntMap.lookup (un (to1dimPos dm p)) $ view _itemMap dm

updateItem :: DungeonMapLike dm => dm -> Pos -> (Item -> IO (Maybe Item)) -> IO dm
updateItem dm p f = alterItem dm p (raiseF f)

alterItem :: DungeonMapLike dm => dm -> Pos -> (Maybe Item -> IO (Maybe Item)) -> IO dm
alterItem dm p f =
  dm & _itemMap (IntMap.alterF f (un (to1dimPos dm p)))



