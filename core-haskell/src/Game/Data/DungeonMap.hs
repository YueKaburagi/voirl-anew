
{-# LANGUAGE OverloadedStrings,TypeFamilies,LambdaCase #-}

module Game.Data.DungeonMap ( DungeonMap(..)
                            , _cache
                            , _pcix
                            , FreezedDungeonMap(..)
                            , genDummyDungeonMap00
                            , genDummyDungeonMap01
                            , updateRoomCachesIfEmpty
                            , getRoomFromPos
                            -- Reexport from DungeonMapLike
                            , getCell
                            , modifyCell
                            , updateCharacter
                            , alterCharacter
                            , getItem
                            , updateItem
                            , alterItem
                            -- For Dump.
                            , getCellsAsDumpBuilder
                            , putExtraInfo
                            ) where

import Relude.Extra.Newtype (un, wrap)

import Data.Default.Class
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, (.:))
import qualified Data.Vector.Mutable as V
import qualified Data.IntMap.Strict as IntMap
import qualified Data.Set as Set
import Control.Lens (Lens', view, over)
import Text.Show (showsPrec)

import Game.Data.Class (HasSaveForm(..))
import Game.Data.DungeonMapLike ( DungeonMapLike(..)
                                , PosCIxMap, CIxPosMap
                                , CharacterMap, ItemMap
                                , FlatCharacterMap, toFlatCharacterMap, fromFlatCharacterMap
                                , FlatPos(..), CharacterIx
                                , to2dimPos, to1dimPos
                                , getCell, modifyCell
                                , updateCharacter, alterCharacter, setCharacter
                                , lookupPlayerCharacterIx
                                , getItem, updateItem, alterItem)
import Game.Data.Pos (Pos(..))
import Game.Data.FloorMap (FloorMap, FreezedFloorMap, _width, _cells, reserveFloorMap)
import Game.Data.Cell (Cell, _cellType, _traits, hasTrait, cellType2SingletonString)
import qualified Game.Data.Cell as C
import Game.Data.Character (Character, isPlayerCharacter, genDummyPlayerCharacter, genDummyCharacter)
import Game.Data.CharacterK.Types (CharacterK)
import Game.Data.Item (Item)
import Game.Data.RoomCache (RoomCache(..), isInternalPos)
import Game.Data.Dump.DumpBuilder (DumpBuilder, FlavoredString(..), _colorFlavor, _string)
import qualified Game.Data.Dump.DumpBuilder as Dump
import Game.Query (QueryDumpControl(..))
import Game.Data.RoomCacheGen (genRoomCache)

data ItemK = ItemK
type CharacterMapK = IntMap CharacterK
type ItemMapK = IntMap ItemK


-- DungoenMapLike が PC の ix を教えてくれるので、cache として預ける。
--  「のりうつり」中にSave/Loadは困ることになるはず
-- Seed から Room が推論できるから cache で預ける。
--  初期状態からの壁の崩し方によっては Room推論が floor-init 時のそれと異なる可能性があったので、これは変更される予定
--  IntMap RoomCache は seed-pos -> RoomCache
data CacheData = CacheData (Maybe CharacterIx) (IntMap RoomCache)
instance Default CacheData where
  def = CacheData Nothing mempty
_pcix :: Lens' CacheData (Maybe CharacterIx)
_pcix f (CacheData x b) = (\a -> CacheData a b) <$> f x
_rooms :: Lens' CacheData (IntMap RoomCache)
_rooms f (CacheData a x) = (\b -> CacheData a b) <$> f x
instance Show CacheData where
  showsPrec _ (CacheData _ roomCaches) s = show (unit <$> IntMap.toList roomCaches) ++ s
    where
      unit :: (Int, RoomCache) -> String
      unit (k, rc) = "(seed: " <> show k <> ", room:" <> show rc <> ")"

data DungeonMap = DungeonMap FloorMap PosCIxMap CIxPosMap CharacterMap ItemMap CacheData
instance DungeonMapLike DungeonMap where
  _floorMap f (DungeonMap x b c d e g) = (\a -> DungeonMap a b c d e g) <$> f x
  _posCIxMap f (DungeonMap a x c d e g) = (\b -> DungeonMap a b c d e g) <$> f x
  _cixPosMap f (DungeonMap a b x d e g) = (\c -> DungeonMap a b c d e g) <$> f x
  _characterMap f (DungeonMap a b c x e g) = (\d -> DungeonMap a b c d e g) <$> f x
  _itemMap f (DungeonMap a b c d x g) = (\e -> DungeonMap a b c d e g) <$> f x
_cache :: Lens' DungeonMap CacheData
_cache f (DungeonMap a b c d e x) = (\g -> DungeonMap a b c d e g) <$> f x

-- | Internal use only.
genMinimumDungeonMap :: FloorMap -> DungeonMap
genMinimumDungeonMap fm =
  DungeonMap fm mempty mempty mempty mempty def

data FreezedDungeonMap = FreezedDungeonMap FreezedFloorMap FlatCharacterMap ItemMap
instance ToJSON FreezedDungeonMap where
  toJSON (FreezedDungeonMap floors characters items) =
    object [ "floor-map" .= floors
           , "characters" .= characters
           , "items" .= items
           ]
instance FromJSON FreezedDungeonMap where
  parseJSON = withObject "FreezedDungeonMap" $
    \v -> FreezedDungeonMap
          <$> v .: "floor-map"
          <*> v .: "characters"
          <*> v .: "items"
instance HasSaveForm DungeonMap where
  type LikeSaveForm DungeonMap = FreezedDungeonMap
  toSaveForm dm@(DungeonMap floors _ _ _ items _) =
    (\x -> FreezedDungeonMap x (toFlatCharacterMap dm) items) <$> toSaveForm floors
  fromSaveForm (FreezedDungeonMap floors characters items) = do
    fm <- fromSaveForm floors
    let (pcm, cpm, cm) = fromFlatCharacterMap characters
    let dm = DungeonMap fm pcm cpm cm items def
    updateRoomCachesIfEmpty $ lookupPCIxIfEmpty dm
    where
      lookupPCIxIfEmpty dm =
        if isJust $ view (_cache . _pcix) dm
        then dm
        else over (_cache . _pcix) (const $ lookupPlayerCharacterIx dm) dm


getRoomFromPos :: Pos -> DungeonMap -> Maybe (FlatPos, RoomCache)
getRoomFromPos p dm =
  IntMap.foldlWithKey f Nothing $ view (_cache . _rooms) dm
  where
    f s n room | isJust s = s
               | isInternalPos room p = Just (FlatPos n, room)
               | otherwise = Nothing


genDummyDungeonMap00 :: IO (Maybe DungeonMap)
genDummyDungeonMap00 = do
  floormap <- reserveFloorMap 16 16
  case floormap of
    Nothing -> pure Nothing
    Just cells -> pure . Just $ genMinimumDungeonMap cells

genDummyDungeonMap01 :: IO (Maybe DungeonMap)
genDummyDungeonMap01 = do
  floormap <- reserveFloorMap width height
  case floormap of
    Nothing -> pure Nothing
    Just fm -> do
      let dm0 = genMinimumDungeonMap fm
      pc <- genDummyPlayerCharacter
      let (dm1, cix) = setCharacter (Pos (3,2)) pc dm0
      let dm2 = over (_cache . _pcix) (const $ Just cix) dm1
      enemy <- genDummyCharacter
      let (dm, _) = setCharacter (Pos (5,2)) enemy dm2
      go 0 (charToCell <$> ss) dm
  where
    (width, height, ss) = sizedPlane planeDummy01
    go :: Int -> [Cell] -> DungeonMap -> IO (Maybe DungeonMap)
    go _ [] dm = pure $ Just dm
    go n (x:xs) dm = do
      modifyCell dm (upPos dm n) (const (pure x))
      go (n+1) xs dm

sizedPlane :: NonEmpty String -> (Int, Int, String)
sizedPlane plane = ( length (head plane)
                   , length plane
                   , concat plane
                   )

planeDummy01 :: NonEmpty String
planeDummy01 =   "################"
               :|
               [ "##....##########"
               , "##*...##########"
               , "##.......#######"
               , "##....##.#######"
               , "########.#######"
               , "########.....###"
               , "############.###"
               , "########.......#"
               , "########..*....#"
               , "########.......#"
               , "################"
               ]

charToCell :: Char -> Cell
charToCell '.' = C.Cell C.CellTypeFloor mempty C.CellFlavor
charToCell '*' = C.Cell C.CellTypeFloor (Set.singleton C.RoomSeed) C.CellFlavor
charToCell _ = C.Cell C.CellTypeWall mempty C.CellFlavor


listAllSeedPosU :: DungeonMap -> IO [Int]
listAllSeedPosU dm =
  V.ifoldl' f [] (view (_floorMap . _cells) dm)
  where
    f :: [Int] -> Int -> Cell -> [Int]
    f l i c | hasTrait C.RoomSeed c = i:l
            | otherwise = l

upPos :: DungeonMap -> Int -> Pos
upPos dm = to2dimPos dm . wrap
downPos :: DungeonMap -> Pos -> Int
downPos dm = un . to1dimPos dm

upRoomCaches :: DungeonMap -> IO DungeonMap
upRoomCaches dm = do
  ps <- fmap (upPos dm) <$> listAllSeedPosU dm
  mrcs <- traverse bindPos ps
  pure $ foldr pushRoomCache dm $ catMaybes mrcs
  where
    bindPos :: Pos -> IO (Maybe (Pos, RoomCache))
    bindPos p = fmap ((,) p) <$> genRoomCache dm p
    pushRoomCache (p,rc) = over (_cache . _rooms) (IntMap.insert (downPos dm p) rc)

isRoomCacheExist :: DungeonMap -> Bool
isRoomCacheExist dm = not . IntMap.null $ view (_cache . _rooms) dm

updateRoomCachesIfEmpty :: DungeonMap -> IO DungeonMap
updateRoomCachesIfEmpty dm | isRoomCacheExist dm = pure dm
                           | otherwise = upRoomCaches dm








-- | For Dump.
getCellsAsDumpBuilder :: DungeonMap -> IO DumpBuilder
getCellsAsDumpBuilder dm = do
  vec <- V.new (V.length $ view _cells fm)
  V.imapM_ (f vec) (view _cells fm)
  return vec
  where
    f v i c | i `mod` w /= 0 = V.write v i $ c2c Dump.OINone c
            | otherwise = V.write v i $ c2c Dump.OIBegginingOfLine c
    c2c o c = FlavoredString Dump.CFNone o $ cellType2SingletonString $ view _cellType c
    fm = view _floorMap dm
    w = view _width fm

-- | For Dump.
--   Mutable Function.
modifyDumpBuilderFromRoomCache :: DungeonMap -> RoomCache -> DumpBuilder -> IO ()
modifyDumpBuilderFromRoomCache dm rCache vec =
  traverse_ (f vec) indices
  where
    l = V.length vec
    indices = [ x | x <- [0..l-1], isInternalPos rCache (upPos dm x) ]
    f v = V.modify v (over _colorFlavor (const Dump.CFBGDullBlue))

-- | For Dump.
modifyDumpBuilderFromPlayerCharacter :: Int -> Character -> DumpBuilder -> IO ()
modifyDumpBuilderFromPlayerCharacter p c vec
  | not (isPlayerCharacter c) = pure ()
  | otherwise = V.modify vec f p
    where
      f = over _string (const "@")
      
-- | For Dump.
modifyDumpBuilderFromCharacter :: Int -> Character -> DumpBuilder -> IO ()
modifyDumpBuilderFromCharacter p c vec
  | isPlayerCharacter c = pure ()
  | otherwise = V.modify vec f p
  where
    f = over _string (const "p")


-- | For Dump.
--   QueryDumpControl に従って DumpBuilder を更新する
--   定義済み処理が行われた場合 Nothing を返し,そうでなかった場合その Just QDC を返す
putExtraInfo :: DungeonMap -> DumpBuilder -> QueryDumpControl -> IO (Maybe QueryDumpControl)
putExtraInfo dm vec =
  \case
    QDCWithKnownRooms -> do
      let roomCaches = view (_cache . _rooms) dm
      traverse_ (\rc -> modifyDumpBuilderFromRoomCache dm rc vec) roomCaches
      pure Nothing
    QDCWithPlayerCharacter -> do
      -- pc-pos から見たら O(1) にできるけど、あえてそうしない
      -- なんらかの影響で PC が2つ以上となった場合にも上手く DUMP させるため
      let cm = toFlatCharacterMap dm
      let ics = IntMap.toList cm
      traverse_ f ics
      pure Nothing
      where
        f (n,c) = modifyDumpBuilderFromPlayerCharacter n c vec
    QDCWithCharacter -> do
      let cm = toFlatCharacterMap dm
      let ics = IntMap.toList cm
      traverse_ f ics
      pure Nothing
      where
        f (n,c) = modifyDumpBuilderFromCharacter n c vec
    other -> pure $ Just other

