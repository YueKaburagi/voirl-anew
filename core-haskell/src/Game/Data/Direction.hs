{-# LANGUAGE OverloadedStrings, LambdaCase #-}

module Game.Data.Direction ( Direction(..)
                           , dirToPos
                           , posToDir
                           , rotateL
                           , rotateR
                           ) where

import Prelude hiding (Either(..), String, Down(..), show)

import Text.Show (show)
import Data.Aeson (FromJSON, parseJSON, ToJSON, toJSON, withText, Value(String))

import Data.Inversity (Inversity(..))
import Game.Data.Pos (Pos(..))



data Direction = Up | Down | Left | Right
                | UpLeft | UpRight
                | DownLeft | DownRight
                deriving (Eq, Enum, Bounded)

instance Show Direction where
  show =
    \case
      Up -> "↑"
      Down -> "↓"
      Left -> "←"
      Right -> "→"
      UpLeft -> "↖"
      UpRight -> "↗"
      DownLeft -> "↙"
      DownRight -> "↘"

dirToPos :: Direction -> Pos
dirToPos =
  \case
    Up -> Pos (0,-1)
    Down -> Pos (0, 1)
    Left -> Pos (-1, 0)
    Right -> Pos ( 1, 0)
    UpLeft -> Pos (-1,-1)
    UpRight -> Pos ( 1,-1)
    DownLeft -> Pos (-1, 1)
    DownRight -> Pos ( 1, 1)

-- | Pos から Directon への変換
--   Pos (0,0) では Nothing
--   境界値処理は 反時計回り
posToDir :: Pos -> Maybe Direction
posToDir (Pos (0,0)) = Nothing
posToDir (Pos (0,y)) | y < 0 = Just Up
                     | otherwise = Just Down
posToDir (Pos (x,y)) =
  -- これ arctan から求めるのとどっちが安いのかな
  -- 右下座標系であることに注意
  case arcNumber (f (abs y) (abs x)) of
    0 | plusY plusX -> Just Right
      | plusY minusX -> Just Left
      | minusY plusX -> Just Right
      | minusY minusX -> Just Left
    1 | plusY plusX -> Just DownRight
      | plusY minusX -> Just DownLeft
      | minusY plusX -> Just UpRight
      | minusY minusX -> Just UpLeft
    2 | plusY plusX -> Just Down
      | plusY minusX -> Just Down
      | minusY plusX -> Just Up
      | minusY minusX -> Just Up
    _ -> Nothing
  where
    f = (/) `on` realToFrac
    tan225 :: Double
    tan225 = 0.4142135 -- tan π/8
    tan675 :: Double
    tan675 = 2.4142135 -- tan 3π/8
    arcNumber :: Double -> Int -- 定義域は [0,π/2)
    arcNumber a | a < tan225 = 0
                | a > tan675 = 2
                | otherwise = 1
    plusY a = y > 0 && a
    minusY a = y < 0 && a
    plusX = x > 0
    minusX = x < 0


rotateR :: Direction -> Direction
rotateR =
  \case
    Up -> UpRight
    Down -> DownLeft
    Left -> UpLeft
    Right -> DownRight
    UpLeft -> Up
    UpRight -> Right
    DownLeft -> Left
    DownRight -> Down

rotateL :: Direction -> Direction
rotateL =
  \case
    Up -> UpLeft
    Down -> DownRight
    Left -> DownLeft
    Right -> UpRight
    UpLeft -> Left
    UpRight -> Up
    DownLeft -> Down
    DownRight -> Right


instance Inversity Direction where
  invert =
    \case
      Up -> Down
      Down -> Up
      Left -> Right
      Right -> Left
      UpLeft -> DownRight
      DownRight -> UpLeft
      UpRight -> DownLeft
      DownLeft -> UpRight

instance FromJSON Direction where
  parseJSON = withText "Direction" $
    \case
      "up" -> pure Up
      "k" -> pure Up
      "down" -> pure Down
      "j" -> pure Down
      "left" -> pure Left
      "h" -> pure Left
      "right" -> pure Right
      "l" -> pure Right
      "up-left" -> pure UpLeft
      "y" -> pure UpLeft
      "up-right" -> pure UpRight
      "u" -> pure UpRight
      "down-left" -> pure DownLeft
      "b" -> pure DownLeft
      "down-right" -> pure DownRight
      "n" -> pure DownRight
      _ -> fail "Unexpected direction code."
instance ToJSON Direction where
  toJSON =
    \case
      Up -> String "up"
      Down -> String "down"
      Left -> String "left"
      Right -> String "right"
      UpLeft -> String "up-left"
      UpRight -> String "up-right"
      DownLeft -> String "down-left"
      DownRight -> String "down-right"

