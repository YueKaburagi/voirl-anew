{-# LANGUAGE OverloadedStrings #-}

module Game.Data.KForCharacter ( DungeonMapKForCharacter(..)
                               , _itselfChK
                               , _characterKs
                               ) where

import Data.Default.Class
import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, withText, (.:?), (.!=))
import Game.Data.CharacterK (CharacterK)
import Control.Lens (Lens')

-- 場合によっては[ChK]以外に、[CellK]や[ItemK]を持つ可能性がある
--- 床落ちアイテムを盗むCとかを想定

data DungeonMapKForCharacter = DungeonMapKForCharacter (Maybe CharacterK) [CharacterK]
instance Default DungeonMapKForCharacter where
  def = DungeonMapKForCharacter Nothing []
instance ToJSON DungeonMapKForCharacter where
  toJSON (DungeonMapKForCharacter selfk cks) =
    object [ "self" .= selfk
           , "cks" .= cks
           ]
instance FromJSON DungeonMapKForCharacter where
  parseJSON = withObject "DungeonMapKForCharacter" $
    \v -> DungeonMapKForCharacter
          <$> v .:? "self"
          <*> v .:? "cks" .!= []


_itselfChK :: Lens' DungeonMapKForCharacter (Maybe CharacterK)
_itselfChK f (DungeonMapKForCharacter x b) = (\a -> DungeonMapKForCharacter a b) <$> f x

_characterKs :: Lens' DungeonMapKForCharacter [CharacterK]
_characterKs f (DungeonMapKForCharacter a x) = (\b -> DungeonMapKForCharacter a b) <$> f x
