module Game.Data.Pos ( Pos(..)
                     , _x
                     , _y
                     , posAround
                     , diffTopLeftSquare
                     , diffTopRightSquare
                     , diffBottomLeftSquare
                     , diffBottomRightSquare
                     , diffCross
                     , diffHorizontal
                     , diffVertical
                     , isDirect
                     , isDirectCross
                     , isDirectDiagonal
                     , distance
                     ) where

import Data.Inversity (invert)

import Game.Data.Pos.Types (Pos(..), _x, _y)



posAround :: Pos -> [Pos]
posAround p = (<> p) <$> diffs
  where
    diffs = [ Pos (-1,-1), Pos ( 0,-1), Pos ( 1,-1)
            , Pos (-1, 0),              Pos ( 1, 0)
            , Pos (-1, 1), Pos ( 0, 1), Pos ( 1, 1)
            ]

diffTopLeftSquare :: Pos -> [Pos]
diffTopLeftSquare p = (<> p) <$> diffs
  where
    diffs = [ Pos (-1,-1), Pos ( 0,-1)
            , Pos (-1, 0)
            ]
diffTopRightSquare :: Pos -> [Pos]
diffTopRightSquare p = (<> p) <$> diffs
  where
    diffs = [ Pos ( 0,-1), Pos ( 1,-1)
            ,              Pos ( 1, 0)
            ]
diffBottomLeftSquare :: Pos -> [Pos]
diffBottomLeftSquare p = (<> p) <$> diffs
  where
    diffs = [ Pos (-1, 0)
            , Pos (-1, 1), Pos ( 0, 1)
            ]
diffBottomRightSquare :: Pos -> [Pos]
diffBottomRightSquare p = (<> p) <$> diffs
  where
    diffs = [              Pos ( 1, 0)
            , Pos ( 0, 1), Pos ( 1, 1)
            ]

diffCross :: Pos -> [Pos]
diffCross p = diffHorizontal p <> diffVertical p

diffVertical :: Pos -> [Pos]
diffVertical p = (<> p) <$> diffs
  where
    diffs = [ Pos (0, -1), Pos (0, 1) ]
diffHorizontal :: Pos -> [Pos]
diffHorizontal p = (<> p) <$> diffs
  where
    diffs = [ Pos (-1, 0), Pos (1, 0) ]


absolute :: Pos -> Int
absolute (Pos (x,y)) = abs x `max` abs y

isDirect :: Pos -> Pos -> Bool
isDirect p0 p1 = 1 == (absolute $ p0 <> invert p1)

isDirectDiagonal :: Pos -> Pos -> Bool
isDirectDiagonal p0 p1 = isDirect p0 p1 && isNormalDiagonal (p0 <> invert p1)
  where
    isNormalDiagonal (Pos (x,y)) = 2 == (abs x + abs y)

isDirectCross :: Pos -> Pos -> Bool
isDirectCross p0 p1 = isDirect p0 p1 && isNormalCross (p0 <> invert p1)
  where
    isNormalCross (Pos (x,y)) = 1 == (abs x + abs y)

distance :: Pos -> Pos -> Int
distance p0 p1 = absolute (p0 <> invert p1)

