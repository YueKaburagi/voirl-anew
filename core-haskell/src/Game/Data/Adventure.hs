{-# LANGUAGE OverloadedStrings,TypeFamilies #-}

module Game.Data.Adventure ( Adventure(..)
                           , AdventureForSave(..)
                           , _dungeonMap
                           , genDummyAdventure
                           ) where

import Data.Aeson (ToJSON, toJSON, object, (.=), FromJSON, parseJSON, withObject, (.:))
import Control.Lens (Lens', view, over)

import Game.Data.Class (HasSaveForm(..))
import Game.Data.DungeonMap (DungeonMap, FreezedDungeonMap, genDummyDungeonMap01, updateRoomCachesIfEmpty)


data Adventure = Adventure DungeonMap
_dungeonMap :: Lens' Adventure DungeonMap
_dungeonMap f (Adventure x) = Adventure <$> f x

data AdventureForSave = AdventureForSave FreezedDungeonMap
instance ToJSON AdventureForSave where
  toJSON (AdventureForSave dungeon) = object [ "dungeon" .= dungeon ]
instance FromJSON AdventureForSave where
  parseJSON = withObject "AdventureForSave" $
    \v -> AdventureForSave <$> v .: "dungeon"
instance HasSaveForm Adventure where
  type LikeSaveForm Adventure = AdventureForSave
  toSaveForm (Adventure dungeon) = AdventureForSave <$> toSaveForm dungeon
  fromSaveForm (AdventureForSave dungeon) = Adventure <$> fromSaveForm dungeon



genDummyAdventure :: IO (Maybe Adventure)
genDummyAdventure = do
  mDM <- genDummyDungeonMap01
  case mDM of
    Nothing -> pure Nothing
    Just dm -> do
      cacheMaked <- updateRoomCachesIfEmpty dm
      pure . Just $ Adventure cacheMaked


