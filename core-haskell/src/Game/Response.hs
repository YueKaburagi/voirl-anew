{-# LANGUAGE OverloadedStrings #-}

module Game.Response ( Response(..)
                     , ResponseBaseMenu(..)
                     , ThinAdventureInfo(..)
                     , AdventureInfo(..)
                     ) where

import Data.Aeson (ToJSON, toJSON, object, (.=))

data Response = ResponseCount Int
              | ResponseBaseMenu ResponseBaseMenu
              deriving (Eq)

instance ToJSON Response where
  toJSON (ResponseCount i) = object [ "count" .= i ]
  toJSON (ResponseBaseMenu r) = toJSON r




data ResponseBaseMenu = ResponseListAdventures [ThinAdventureInfo]
                      | ResponseInfoAdventure AdventureInfo
                      deriving (Eq)
instance ToJSON ResponseBaseMenu where
  toJSON (ResponseListAdventures list) = toJSON list
  toJSON (ResponseInfoAdventure info) = toJSON info


data ThinAdventureInfo = ThinAdventureInfo Int Text Text
                       deriving (Eq)
instance ToJSON ThinAdventureInfo where
  toJSON (ThinAdventureInfo sortKey ix name) =
    object [ "sort-key" .= sortKey
           , "adventure-ix" .= ix
           , "name" .= name ]

data AdventureInfo = AdventureInfo Text Text Bool Text
                   deriving (Eq)
instance ToJSON AdventureInfo where
  toJSON (AdventureInfo ix name clearFlag desc) =
    object [ "adventure-ix" .= ix
           , "name" .= name
           , "clear-flag" .= clearFlag
           , "description" .= desc
           ]
