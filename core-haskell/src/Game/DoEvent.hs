{-# LANGUAGE OverloadedStrings, LambdaCase, RankNTypes, ScopedTypeVariables #-}

module Game.DoEvent ( Diff
                    , HasRandomGen(..)
                    , PartialTurnState(..)
                    , mkPartialTurnState
                    , PhKPred
                    , getSense
                    , sense
                    , phkpredToText
                    , doEventMove1Character
                    , helperTakeAllEnemyCharacterExistentials
                    , helperUpdateAllSights
                    , doDummyEventBeforeEnemyPhase
                    , doAllEnemiesActions
                    ) where

import Relude.Extra.Newtype (un)

import Data.Default.Class (Default(..))
import Control.Lens (Lens', view, over, use, modifying, assign)
import qualified Data.IntMap.Strict as IntMap
import qualified Data.Map.Strict as Map
import System.Random (StdGen, UniformRange, uniformR)

import Data.Inversity (invert)
import Game.Data.Pos (Pos, distance)
import Game.Data.FloorMap (FloorMap, _cells, isValidPosition)
import Game.Data.Cell (Cell, _cellType, CellType(..))
import Game.Data.Character (Character(Character), isPlayerCharacter)
import Game.Data.KForCharacter (_itselfChK, _characterKs)
import Game.Data.CharacterK (_chkPos, _chkFaction, Faction(..))
import Game.Data.Item (Item)
import Game.Data.DungeonMapLike (DungeonMapLike(..), FlatPos, CharacterIx, getCell
                                , CharacterFullSet
                                , getChAndPosFromIx
                                , getAllChsAndPosBy
                                , getAllChsFullSetBy
                                , getCharacterIxFromPos
                                , getCharacterPosFromIx
                                , getCharacterOthersFromPos
                                , getCharacterOthersFromIx
                                , mkDiffReplaceCharacter
                                , mkDiffUpdateAllCharactersWithPos
                                , modify2CharacterWithFullSetM
                                )
import Game.Data.DungeonMap (DungeonMap, getRoomFromPos)
import Game.Data.RoomCache (RoomCache, isInternalPos)

import Game.Data.Direction (Direction, dirToPos, posToDir)
import qualified Game.Data.Direction as Dir


-- | 対象のCellが進入可能属性かどうか
isEnterableCell :: DungeonMapLike dm => Character -> Pos -> dm -> IO Bool
isEnterableCell _ p dm | not (isValidPosition (view _floorMap dm) p) = pure False
isEnterableCell ch p dm = do
  c <- getCell dm p
  case view _cellType c of
    CellTypeFloor -> pure True
    CellTypeWall -> pure False

-- | 対象のCellにCharacterが入っていないか
isOpenCell :: DungeonMapLike dm => Pos -> dm -> Bool
isOpenCell p dm =
  isNothing $ getCharacterIxFromPos dm p


-- -- 将来的に O(N・M) になる問題があるのですけれど
-- --  AABB のような発想で N か M を小さくするか
-- --  なんとか上手いことして logN か logM が作れれば多少マシだね
-- --  あとは何らかの手段で GPGPU に N か M を並列化した状態で渡せればとか
-- 考えておきましょう
senseB :: Pos -> CharacterIx -> Sense -> SensePack -> Bool
senseB p0 cix0 (Sense sight) (SensePack ty emitter) =
  case emitter of
    PKEmitterVision p1 ->
      case sight of
        SightCircle r -> distance p0 p1 <= r
        SightRoom r -> isInternalPos r p1
    PKEmitterSelf cix1 ->
      cix0 == cix1

-- 1 つの Event が生成する [PhenomenonK] の数は高々十数個の想定
sense :: Pos -> CharacterIx -> Sense -> [PhenomenonK] -> [PhKPred]
sense p cix s dqs0 =
  fmap extract . snd . foldl' f ([],[]) $ dqs1
  where
    dqs1 = filter filter1 dqs0
    filter1 (PhK sp _ _) = senseB p cix s sp
    f (denyixs,passed) x@(PhK _ _ ix) =
      if elem ix denyixs
      then (denyixs,passed)
      else (ix:denyixs, x:passed)
    extract (PhK _ phkp _) = phkp


data Sense = Sense Sight
data SenseType = Self
               | Primitive
               | Vision
               | Touch
               -- 動体感知, アイテム感知, 罠感知, など、いろいろ増やす気がする
               deriving (Eq, Ord)
data SensePack = SensePack SenseType PhKEmitter
-- これ SenseType項 要らないのでは？

data PhKEmitter = PKEmitterSelf CharacterIx
                | PKEmitterVision Pos
                deriving (Eq)


-- | Mock
getSense :: Character -> Sense
getSense _ = Sense (SightCircle 4)

-- | Mock
updateSight :: Sight -> Character -> Character
updateSight _ = id

-- | Mock
cogCharacter :: cog -> Character -> PhKCharacter
cogCharacter _ c | isPlayerCharacter c = PhKCharacter FIxPlayer (FormIx "@") mempty
                 | otherwise = PhKCharacter FIxEnemy (FormIx "c") mempty

-- character は sight を 揮発性データとして持ち、savedataのload後に sight 更新処理が走るのを期待する
-- ？
-- character が持つ 不揮発性データ って？
--  例えば C が持つ、行動指針。(直前にどのマスから進んだかとか。自身が逃走モードであるか否かとか)
--  例えば @ が持つ、……？？
--   Advで蓄積した K は Adv側に預けるし、受け取った TextLog も Adv側 だよね？
--   HP/満腹度などの一部パラメータは @自身 が持っていると素直な気がする

data Sight = SightRoom RoomCache -- | 部屋指定での視界。部屋の中では通常こちらを優先
           | SightCircle Int -- | 半径指定での視界。通常 1。霊体は 2以上かも
           deriving (Eq) -- SightProvidence -- | いわゆる透視。(各種感知とは別？)
type Diff a = a -> IO a
type PhKIx = Int -- | 単一ターンに生成された PhKPred の中で unique な識別子
data PhKPred = PKPStandardMoved Pos Direction -- | 移動原点とそこからの移動方向
             | PKPExistence Pos PhKCharacter -- | 現在位置
             | PKPNormalSight Sight -- | 視界更新
             | PKPAttackMotion Pos Direction -- | 攻撃モーション
             | PKPAttackEffect Pos Direction AttackEffectType -- | 攻撃範囲エフェクト
             | PKPGotDamage Pos -- | 被弾モーション
             | PKPAttackEvaded Pos -- | 攻撃スカり。攻撃者の位置に発生
             | PKPGiveDamage Int -- | ダメージ量

newtype AttackEffectType = AttackEffectType Text deriving (Show, Eq)

data FactionIx = FIxPlayer | FIxEnemy deriving (Eq) -- Marchant?
newtype FormIx = FormIx Text deriving (Show, Eq, Ord)
data BadStatus = BSUnknown deriving (Show, Eq)
data PhKCharacter = PhKCharacter FactionIx FormIx [BadStatus] -- 向きは現時点では含めない

-- PKPCharactersInSight -- | 常在|ターン終了 で返す想定。見えてる character を全て返す？

-- `pushPhK` で都合がいいので PhKIx は末尾が望ましい
data PhenomenonK = PhK SensePack PhKPred PhKIx

fictionixToText :: FactionIx -> Text
fictionixToText =
  \case
    FIxPlayer -> "@"
    FIxEnemy -> "e"

phkcToText :: PhKCharacter -> Text
phkcToText (PhKCharacter ficix formix bss) =
  "PhKC(t:" <> fictionixToText ficix <> " f:" <> un formix <> "s:" <> show bss <> ")"

-- 主に server 側で phk を dump する時に使う想定
phkpredToText :: PhKPred -> Text
phkpredToText =
  \case
    PKPStandardMoved p dir -> "StdMov o:" <> show p <> " d:" <> show dir <> " "
    PKPExistence p c -> "∃ p:" <> show p <> " c:" <> phkcToText c
    PKPNormalSight s ->
      case s of
        SightRoom r -> "Sight/R " <> show r
        SightCircle r -> "Sight/C " <> show r
    PKPAttackMotion p dir -> "AtkMo o:" <> show p <> " d:" <> show dir <> " "
    PKPAttackEffect p dir eff -> "AtkEf o:" <> show p <> " d:" <> show dir <> "  e:" <> show eff
    PKPAttackEvaded p -> "AtkEvd o:" <> show p
    PKPGotDamage p -> "Dmgd o:" <> show p
    PKPGiveDamage n -> "Dmg " <> show n

class HasRandomGen a where
  _randomGen :: Lens' a StdGen

instance HasRandomGen StdGen where
  _randomGen f a = f a


data PartialTurnState = PartialTurnState StdGen PhKIx DungeonMap [PhenomenonK]
mkPartialTurnState :: StdGen -> DungeonMap -> PartialTurnState
mkPartialTurnState rgen dm = PartialTurnState rgen 0 dm mempty

instance HasRandomGen PartialTurnState where
  _randomGen f (PartialTurnState x b c d) = (\a -> PartialTurnState a b c d) <$> f x
_nextPhKIx :: Lens' PartialTurnState PhKIx
_nextPhKIx f (PartialTurnState a x c d) = (\b -> PartialTurnState a b c d) <$> f x
_dungeonMap :: Lens' PartialTurnState DungeonMap
_dungeonMap f (PartialTurnState a b x d) = (\c -> PartialTurnState a b c d) <$> f x
_revPhKs :: Lens' PartialTurnState [PhenomenonK]
_revPhKs f (PartialTurnState a b c x) = (\d -> PartialTurnState a b c d) <$> f x

type PartialTurnST = StateT PartialTurnState IO


genTurnUniquePhKIx :: PartialTurnST PhKIx
genTurnUniquePhKIx = do
  n <- use _nextPhKIx
  modifying _nextPhKIx (+1)
  pure n

pushPhKs :: [PhKIx -> PhenomenonK] -> PartialTurnST ()
pushPhKs fs = do
  phkix <- genTurnUniquePhKIx
  modifying _revPhKs (fmap (phkix &) fs <>)

pushPhK :: (PhKIx -> PhenomenonK) -> PartialTurnST ()
pushPhK f = pushPhKs (pure f)


-- PartialTurnST の処理で、
-- Maybe を 何度も case of で拾って階層がどんどん深くなるので、ちょっと緩やかにしたい
continuous :: Applicative f => a -> (Maybe b) -> (b -> f a) -> f a
continuous a Nothing _ = pure a
continuous _ (Just x) f = f x

continuousB :: Applicative f => a -> Bool -> f a -> f a
continuousB a b g | b = g
                  | otherwise = pure a

continuousM :: Monad m => a -> m (Maybe b) -> (b -> m a) -> m a
continuousM a mb f = do
  q <- mb
  continuous a q f

-- False を 吸収元 とした magma っぽく見える
infix 1 ??>
(??>) :: Applicative f => Maybe b -> (b -> f Bool) -> f Bool
(??>) = continuous False
{-# INLINE (??>) #-}

infix 1 ??>=
(??>=) :: Monad m => m (Maybe b) -> (b -> m Bool) -> m Bool
(??>=) = continuousM False
{-# INLINE (??>=) #-}

infixr 0 &&>
(&&>) :: Applicative f => Bool -> f Bool -> f Bool
(&&>) = continuousB False
{-# INLINE (&&>) #-}

-- `bool &&> maybe ??> \a -> do` = `bool &&> (maybe ??> \a -> do)`
-- `bool &&> bool &&> do` = `bool &&> (bool &&> do)`


commonMove :: CharacterIx -> Direction -> PartialTurnST Bool
commonMove cix dir = do
  dm <- use _dungeonMap
  getCharacterOthersFromIx dm cix ??>
    \(c,p) -> do
      let nextp = p <> dirToPos dir
      canEnter <- liftIO $ isEnterableCell c nextp dm
      (canEnter && isOpenCell nextp dm) &&> do
        modifying _dungeonMap $ mkDiffReplaceCharacter nextp cix
        -- Walked
        let phk1 = PKPStandardMoved p dir
        pushPhKs [ PhK (SensePack Vision $ PKEmitterVision nextp) phk1
                 , PhK (SensePack Vision $ PKEmitterVision p) phk1
                 ]
        pure True

-- これ Log.msg 系を使いたかったりする？
-- | Character を 指定の方向へ 1マス移動 させる event
--   Character が存在して 1マス移動 に成功したならば True
--   そうでなければ False を返す
doEventMove1Character :: CharacterIx -> Direction -> PartialTurnST Bool
doEventMove1Character cix dir = do
  b <- commonMove cix dir
  if not b
    then pure b
    else 
          -- ↓移動後汎用処理とできるのでは？
          --  罠を trigger するかしないか で2種類欲しくない？
          --  PC か C かによっても処理が少し異なるかも
          -- Existence
          -- このタイミングでは送らない
          -- Existence と Sight は @ の場合だけ送りたい気がする
          -- 
          -- pushPhK $ PhK (SensePack Self $ PKEmitterSelf cix) (PKPExistence nextp)
          -- Sight
          -- let room = snd <$> getRoomFromPos nextp dm
          -- pushPhK $ PhK (SensePack Self $ PKEmitterSelf cix) $
          --       case room of
          --         Nothing -> PKPNormalSight (SightCircle 1)
          --         Just r -> PKPNormalSight (SightRoom r)
          -- gen PhK: 通常移動, p-origin, dir. 視覚:0
          -- gen PhK: 存在, nextp. 常在. 自己認識:0
          -- gen PhK: 足元罠発見, nextp. TextLog.? 視覚:0, 触覚:0
          -- gen PhK: 足元罠無し, nextp. 触覚:0
          -- gen PhK: 視界, nextp. 常在. 単純認識:0
          -- gen PhK: 足元アイテム乗り, nextp. TextLog.? 視覚:0, 触覚:0
          -- gen Evn: 足元罠発動
          pure True

-- | return as diff
helperUpdateAllSights :: PartialTurnST ()
helperUpdateAllSights = do
  dm <- use _dungeonMap
  let mkSight c p =
        case snd <$> getRoomFromPos p dm of
          Just r -> SightRoom r
          Nothing -> SightCircle 1
  let f c p = updateSight (mkSight c p) c
  modifying _dungeonMap $ mkDiffUpdateAllCharactersWithPos f
    
-- | return as phk
--   EnemyCharacter requires c ∈ {@}. 
helperTakeAllEnemyCharacterExistentials :: PartialTurnST ()
helperTakeAllEnemyCharacterExistentials = do
  -- PhKIx は Existence毎 に1つ在るべきなので pushPhK を traverse_ させる
  dm <- use _dungeonMap
  traverse_ pushPhK $ fmap f $ getAllChsAndPosBy dm (isPlayerCharacter)
  where
    mkSPack p = SensePack Vision $ PKEmitterVision p
    f (c,p) = PhK (mkSPack p) (PKPExistence p $ cogCharacter () c)


doDummyEventBeforeEnemyPhase :: PartialTurnST ()
doDummyEventBeforeEnemyPhase =
  helperUpdateAllSights >> helperTakeAllEnemyCharacterExistentials


doAllEnemiesActions :: PartialTurnST ()
doAllEnemiesActions = do
  pas <- genAllEnemiesPrioritizedActions
  traverse_ (uncurry doPrioritizedActions) pas

doPrioritizedActions :: CharacterIx -> [PrioritizedAction] -> PartialTurnST Bool
doPrioritizedActions _ [] = pure False
doPrioritizedActions cix (pa:rest) = do
  b <- doPrioritizedAction cix pa
  if b
    then pure b
    else doPrioritizedActions cix rest

genAllEnemiesPrioritizedActions :: PartialTurnST [(CharacterIx, [PrioritizedAction])]
genAllEnemiesPrioritizedActions = do
  dm <- use _dungeonMap
  let characterPacks = getAllChsFullSetBy dm (not . isPlayerCharacter)
  traverse genPack characterPacks
  where
    genActions (c, p, cix) = generatePrioritizedActions c
    genPack a@(_, _, cix) = (,) <$> pure cix <*> genActions a


commonMeleeAttackIf :: (Character -> Bool) -> CharacterIx -> Direction -> PartialTurnST Bool
commonMeleeAttackIf f cix dir = do
  dm <- use _dungeonMap
  getCharacterPosFromIx dm cix ??>
    \p ->
      let nextp = p <> dirToPos dir
      in
        getCharacterOthersFromPos dm nextp ??>
        \(targetC, targetCIx) ->
          f targetC &&>
          modify2CharacterWithFullSetM (runnerNormalAttack dir) cix targetCIx dm ??>=
          \(newDM, _) -> do
            assign _dungeonMap newDM
            pure True
                  -- 死亡時誘発や、非ダメージ時誘発ってどのタイミング？
                  -- 処理に割り込んだほうがそれっぽい？



-- | IMCOMPLETED
--   敵の意思決定に使用する用
getDirectionInSightHostile :: Character -> PartialTurnST (Maybe Direction)
getDirectionInSightHostile (Character dmk) =
  let
    cks = view _characterKs dmk
    candies = filter (\k -> PlayerFaction == view _chkFaction k) cks
    mixed = (,) <$> view _itselfChK dmk <*> (fmap fst . uncons) candies -- 本来なら乱数でtakeOneする
  in
    case mixed of
      Nothing -> pure Nothing
      Just (selfK, targetK) ->
        let
          sp = view _chkPos selfK
          tp = view _chkPos targetK
          dp = tp <> invert sp
        in
          pure $ posToDir dp
getDirectionInSightHostile _ = pure $ Just Dir.Left

-- | MOCK UP
getDirectionLastMove :: Character -> PartialTurnST (Maybe Direction)
getDirectionLastMove c = pure Nothing

-- | 行動予定を組み立てる
generatePrioritizedActions :: Character -> PartialTurnST [PrioritizedAction]
generatePrioritizedActions c = do
  dirM <- liftA2 (<|>) (getDirectionInSightHostile c) $ getDirectionLastMove c
  dir <- case dirM of
    Just d -> pure d
    Nothing -> randomEnum
  pure [PAStepOrAttack isPlayerCharacter dir]


-- | 非@Cが発行する行動予定
data PrioritizedAction = PAStepOrAttack (Character -> Bool) Direction

-- | 行動予定を処理させる
doPrioritizedAction :: CharacterIx -> PrioritizedAction -> PartialTurnST Bool
doPrioritizedAction cix = 
  \case
    PAStepOrAttack isHostile dir -> do
      isMoveSucceed <- commonMove cix dir
      if isMoveSucceed
        then do
        -- doCommon Enemy Enter Cell
        pure True
        else commonMeleeAttackIf isHostile cix dir

-- | ランダムな x を返す。 x ∈ [最小値, 最大値]
randomR :: (HasRandomGen s, Monad m, UniformRange a) => a -> a -> StateT s m a
randomR min max = do
  g <- use _randomGen
  let (a, ng) = uniformR (min, max) g
  assign _randomGen ng
  pure a


-- | ランダムな e を返す。 e ∈ {Enum}
randomEnum :: forall a g m s. (HasRandomGen s, Monad m, Enum a, Bounded a) => StateT s m a
randomEnum =
  toEnum <$> randomR (fromEnum (minBound :: a)) (fromEnum (maxBound :: a))
-- minBound, maxBoundの型が定まらないので、 ScopedTypeVariables拡張から 型を束縛できるようにする必要がある


runnerNormalAttack :: Direction
                   -> CharacterFullSet -> CharacterFullSet
                   -> PartialTurnST (Character, Character, ())
runnerNormalAttack dir publisher target = do
  pushPhK $ PhK (mkVSP pP) $ PKPAttackMotion pP dir
  pushPhK $ PhK (SensePack Vision $ PKEmitterVision pT) $ PKPAttackEffect pT dir (AttackEffectType "hoge")
  accuracy <- randomR 0.0 1.0
  if accuracy > (0.95 :: Float)
    then do
    pushPhK $ PhK (mkVSP pT) $ PKPAttackEvaded pP
    pure (cP, cT, ())
    else do
    pushPhK $ PhK (mkVSP pT) $ PKPGotDamage pT
    let atkP = mockGetCharacterParam (ParamIx "atk") cP
    let defP = mockGetCharacterParam (ParamIx "def:physic") cT
    let damage = max 1 (atkP - defP)
    pushPhK $ PhK (mkVSP pT) $ PKPGiveDamage damage
    -- ここに hp 減算処理とか
    pure (cP, cT, ())
  where
    (cP, pP, cixP) = publisher
    (cT, pT, cixT) = target
    mkVSP = SensePack Vision . PKEmitterVision



mockNormalAttack :: Direction
                 -> (CharacterFullSet, CharacterFullSet) -> (Character, Character, PartialTurnST ())
mockNormalAttack dir (publisher, target) = (cP, cT, genAttackPhKs)
  where
    (cP, pP, cixP) = publisher
    (cT, pT, cixT) = target
    genAttackPhKs = do
      pushPhK $ PhK (SensePack Vision $ PKEmitterVision pP) $ PKPAttackMotion pP dir
      pushPhK $ PhK (SensePack Vision $ PKEmitterVision pT) $ PKPAttackEffect pT dir (AttackEffectType "hoge")

newtype ParamIx = ParamIx Text deriving (Show, Eq)
mockGetCharacterParam :: ParamIx -> Character -> Int
mockGetCharacterParam pix _ =
  case pix of
    ParamIx "atk" -> 8
    ParamIx "hp" -> 3
    ParamIx "def:physic" -> 1
    _ -> 0

mockIsDied :: Character -> Bool
mockIsDied = const False

