{-# LANGUAGE OverloadedStrings, LambdaCase #-}

module Game.Query ( Query(..)
                  , QueryBaseMenu(..)
                  , QueryAdventure(..)
                  , QueryPlayerCharacter(..)
                  , QueryDebug(..)
                  , QueryDumpType(..)
                  , QueryDumpControl(..)
                  ) where

import Data.Aeson (FromJSON, parseJSON, withObject, withText, (.:), (.:?), (.!=))
import Data.Aeson.Types (Parser)

import Game.Data.Direction (Direction)

data Query = QueryCountUp
           | QueryBaseMenu QueryBaseMenu
           | QueryAdventure QueryAdventure
           | QueryPlayerCharacter QueryPlayerCharacter
           | QueryDebug QueryDebug
           deriving (Eq)
instance FromJSON Query where
  parseJSON x = pCommon x <|> pBaseMenu x <|> pAdventure x <|> pPC x <|> pDebug x
    where
      pCommon = withObject "Game.Query" $
        \v -> do
          cmd <- (v .: "cmd" :: Parser Text)
          case cmd of
            "count-up" -> pure QueryCountUp
            _ -> fail "unexpecetd cmd"
      pBaseMenu v = QueryBaseMenu <$> parseJSON v
      pAdventure v = QueryAdventure <$> parseJSON v
      pPC v = QueryPlayerCharacter <$> parseJSON v
      pDebug v = QueryDebug <$> parseJSON v


data QueryDebug = DebugQueryDump QueryDumpType [QueryDumpControl]
                deriving (Eq)
instance FromJSON QueryDebug where
  parseJSON = withObject "Game.DebugQuery" $
    \v -> do
      debug <- (v .: "debug" :: Parser Text)
      case debug of
        "dump" -> do
          t <- v .: "type"
          cs <- v .:? "options" .!= []
          pure $ DebugQueryDump t cs
        _ -> fail "Unexpected debug command."

data QueryDumpType = QDTUnknown Text
                   | QDTCurrentAdventureFloorMap
                   | QDTCurrentAdventureCacheData
                   deriving (Eq)
instance FromJSON QueryDumpType where
  parseJSON = withText "Game.QueryDumpType" $
    \case
      "adv-floor" -> pure QDTCurrentAdventureFloorMap
      "adv-cache" -> pure QDTCurrentAdventureCacheData
      other -> pure $ QDTUnknown other

data QueryDumpControl = QDCUnknown Text
                      | QDCWithKnownRooms
                      | QDCWithPlayerCharacter
                      | QDCWithCharacter
                      deriving (Eq, Show)
instance FromJSON QueryDumpControl where
  parseJSON = withText "Game.QueryDumpControl" $
    \case
      "rooms" -> pure QDCWithKnownRooms
      "pc" -> pure QDCWithPlayerCharacter
      "chars" -> pure QDCWithCharacter
      other -> pure $ QDCUnknown other


data QueryBaseMenu = QueryListAdventures
                   | QueryInfoAdventure Text
                   | QueryEnterAdventure Text
                   deriving (Eq)
instance FromJSON QueryBaseMenu where
  parseJSON = withObject "Game.QueryBaseMenu" $
    \v -> do
      cmd <- (v .: "cmd" :: Parser Text)
      case cmd of
        "list-adventures" -> pure QueryListAdventures
        "info-adventure" -> QueryInfoAdventure <$> v .: "adventure-ix"
        "enter-adventure" -> QueryEnterAdventure <$> v .: "adventure-ix"
        _ -> fail "Unexpected cmd"


data QueryAdventure = QueryAbortAdventure
                    deriving (Eq)
instance FromJSON QueryAdventure where
  parseJSON = withObject "Game.QueryAdventure" $
    \v -> do
      cmd <- (v .: "cmd" :: Parser Text)
      case cmd of
        "abort-adventure" -> pure QueryAbortAdventure
        _ -> fail "Unexpected cmd"


data QueryPlayerCharacter = QueryPCMove Direction
                          deriving (Eq)
instance FromJSON QueryPlayerCharacter where
  parseJSON = withObject "Game.QueryPlayerCharacter" $
    \v -> do
      cmd <- (v .: "cmd" :: Parser Text)
      case cmd of
        "move" -> QueryPCMove <$> v .: "dir"
        _ -> fail "Unexpected cmd."
