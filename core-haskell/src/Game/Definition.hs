{-# LANGUAGE OverloadedStrings #-}

module Game.Definition ( _version
                       , GameSystemDefinition(..)
                       , GameDefinition(..)
                       ) where

import Relude.Extra.Newtype (un)

import Data.Default.Class
import Data.Aeson (FromJSON, ToJSON, toJSON, parseJSON, withObject, withText, object, (.:), (.:?), (.!=))
import Data.Aeson.Types (Parser)
import qualified Data.Aeson as JSON
import Control.Lens (Lens', Getter, to, view)

import Game.Version (VersionString(..))


class LikeDefinition a where
  _version :: Getter a VersionString
  versionStringOnly :: VersionString -> a
  -- | 定義が真に読み込まれているかどうか。version string のみを save data から復帰させるので
  isLoaded :: a -> Bool


-- | savedata が読んでいた定義と、system が起動時に読んだ定義の version が合っているかをcheckする目的
sameVersion :: LikeDefinition a => a -> a -> Bool
sameVersion a b = view _version a == view _version b

helperDefinitionToJSON :: LikeDefinition a => a -> JSON.Value
helperDefinitionToJSON = JSON.String . un . view _version
helperDefinitionFromJSON :: LikeDefinition a => String -> JSON.Value -> Parser a
helperDefinitionFromJSON name = withText name $
   pure . versionStringOnly . VersionString

data GameSystemDefinition = GameSystemDefinition VersionString deriving (Eq) -- mock
instance Default GameSystemDefinition where
  def = versionStringOnly (VersionString "null")
instance LikeDefinition GameSystemDefinition where
  _version = to (const $ VersionString "0.0.0")
  versionStringOnly v = GameSystemDefinition v
  isLoaded (GameSystemDefinition _) = True
instance ToJSON GameSystemDefinition where
  toJSON = helperDefinitionToJSON
instance FromJSON GameSystemDefinition where
  parseJSON = helperDefinitionFromJSON "GameSystemDefinition"



data GameDefinition = GameDefinition VersionString deriving (Eq) -- mock
instance Default GameDefinition where
  def = versionStringOnly (VersionString "null")
instance LikeDefinition GameDefinition where
  _version = to (const $ VersionString "0.0.0")
  versionStringOnly v = GameDefinition v
  isLoaded (GameDefinition _) = True
instance ToJSON GameDefinition where
  toJSON = helperDefinitionToJSON
instance FromJSON GameDefinition where
  parseJSON = helperDefinitionFromJSON "GameDefinition"

