{-# LANGUAGE OverloadedStrings #-}

module Lib
    ( mainMode
    , genDefaultConfigFile
    ) where

import Relude.Extra.Newtype (un)

import Data.Default.Class
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import Network.Wai.Handler.Warp (run)
import Network.Wai (Application, responseLBS, getRequestBodyChunk)
import Network.HTTP.Types.Status (status200)
import Network.HTTP.Types.Header (hContentLength, ResponseHeaders)
import Control.Concurrent (forkIO, killThread, threadDelay, ThreadId)
import Data.Aeson (FromJSON, ToJSON)
import Data.YAML.Aeson (decode1, encode1)
import System.Directory (doesFileExist)

import Configure.Common (relatedFilename)
import Configure.System (SysConf, serverLifeTimeMicroSeconds, serverPort, loaderToConf)
import Game (game, dataToSave)
import Game.System (actionEndLifetime, WithLogger(..))
import Response (packResponse, PackedResponse(..), ResponseBody(..), ResponseSchemeName(..), Response, isPlannedExit)
import Query (bytestringToQuery, isValidQuery)
import Game.State.GameState (GameSystemState(..))
import qualified Log
import Log (Logger)


mainMode :: IO ()
mainMode = do
  gameState <- newMVar (def :: GameSystemState)
  exitMark <- newMVar True
  l <- Log.prepareLogger
  syscfg <- loadSystemConfigure l
  serverId <- forkIO $ run (serverPort syscfg) (server l exitMark gameState)
  observer l syscfg exitMark gameState serverId
  Log.endLogger l

genDefaultConfigFile :: IO ()
genDefaultConfigFile =
  let
    relPath = "../" <> relatedFilename (Proxy :: Proxy SysConf)
    syscfg = def :: SysConf
  in
    saveToYaml relPath syscfg


loadSystemConfigure :: Logger -> IO SysConf
loadSystemConfigure l = do
  let fileName = relatedFilename (Proxy :: Proxy SysConf)
  let relPath = "../" <> fileName
  isSysCfgExist <- doesFileExist relPath
  if isSysCfgExist
    then do
    loader <- loadFromYamlOrElse l def relPath
    let rightLogger s = Log.info l $ "[" <> fileName <> "] Use '" <> s <> "' in configuration file."
    let leftLogger s = Log.warn l $ "[" <> fileName <> "] Use default '" <> s <> "'. No such configuration found."
    loaderToConf leftLogger rightLogger loader
    else do
    Log.warn l ("Missing '" <> fileName <> "'. Start with default settings." :: String)
    pure def

loadFromYamlOrElse :: FromJSON a => Logger -> a -> FilePath -> IO a
loadFromYamlOrElse l d path = do
  bs <- L.readFile path
  case decode1 bs of
    Left (p, s) -> do
      Log.err l ("[" <> path <> "] Parse error (" <> show p <> ")\n" <> show s)
      pure d
    Right x ->
      pure x

saveToYaml :: ToJSON a => FilePath -> a -> IO ()
saveToYaml path a =
  L.writeFile path (encode1 a)


observer :: Logger -> SysConf -> MVar ExitMark -> MVar GameSystemState -> ThreadId -> IO ()
observer logger syscfg exitMark gameState tid = do
  threadDelay $ serverLifeTimeMicroSeconds syscfg
  isExit <- swapMVar exitMark True
  if isExit
    then do
    gs <- readMVar gameState
    (_response, _) <- runStateT (actionEndLifetime dataToSave) (WithLogger logger gs)
    _response `seq` killThread tid
    else observer logger syscfg exitMark gameState tid


type ExitMark = Bool

server :: Logger -> MVar ExitMark -> MVar GameSystemState -> Application
server logger exitMark gameState req respond = do
  bs <- getRequestBodyChunk req
  let query = bytestringToQuery bs
  gs <- readMVar gameState
  _ <- swapMVar exitMark . not . isValidQuery $ query
  (r, WithLogger _ newState) <- runStateT (game query) (WithLogger logger gs)
  let isEXIT = isPlannedExit r
  _ <- if isEXIT
    then swapMVar exitMark True
    else pure True
  _ <- swapMVar gameState newState
  let (header, payload) = genHeaderAndLPayload r
  respond $ responseLBS status200 header payload

genHeaderAndLPayload :: ToJSON r => Response r -> (ResponseHeaders, L.ByteString)
genHeaderAndLPayload r =
  let
    (PackedResponse scheme body) = packResponse r
    size = show $ B.length (un body)
  in
    (,) [ (hContentLength, size)
    , ("Scheme", un scheme)
    ] $ L.fromStrict (un body)


