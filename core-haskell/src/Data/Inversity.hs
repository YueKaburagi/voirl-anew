
module Data.Inversity (Inversity(..)) where

class Inversity a where
  invert :: a -> a


instance Inversity () where
  invert () = ()

instance Inversity Bool where
  invert = not
