module Configure.Common ( HasRelatedFilename(..) ) where

class HasRelatedFilename a where
  relatedFilename :: Proxy a -> FilePath

