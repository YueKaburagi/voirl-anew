{-# LANGUAGE OverloadedStrings, TemplateHaskell,RankNTypes,LambdaCase,ExistentialQuantification #-}

module Configure.System ( SysConf(..)
                        , SysConfLoader
                        , serverLifeTimeMicroSeconds
                        , loaderToConf
                        ) where


import Data.Default.Class
import Network.Wai.Handler.Warp (Port)

import Data.Aeson (ToJSON, FromJSON, parseJSON, toJSON, (.:?), withObject, object, (.=))
import qualified Data.Aeson as JSON
import qualified Data.Aeson.Key as JSON
import Data.Aeson.Types (Parser, Pair)
import Control.Lens.TH (makeLenses, makeLensesFor)
import Control.Lens (Lens', over, view, set)

import Configure.Common ( HasRelatedFilename(..) )


data SysConf = SysConf
  { serverLifeTimeSeconds :: Int
  , serverPort :: Port
  }
  deriving (Eq, Show)
makeLensesFor [( "serverLifeTimeSeconds" , "_serverLifeTimeSeconds" )
              ,( "serverPort" , "_serverPort" )] ''SysConf
instance Default SysConf where
  def = SysConf
    { serverLifeTimeSeconds = 12 * 60
    , serverPort = 1234
    }

data SysConfLoader = SCL
  { _sclServerLifetimeSeconds :: Either Int Int
  , _sclServerPort :: Either Port Port
  }
  deriving (Eq, Show)
makeLenses ''SysConfLoader
instance Default SysConfLoader where
  def = SCL
    { _sclServerLifetimeSeconds = useOriginal serverLifeTimeSeconds
    , _sclServerPort = useOriginal serverPort
    }
    where
      useOriginal :: (SysConf -> a) -> Either a a
      useOriginal f = Left $ f def



data Companion a = Companion String (Lens' SysConf a) (Lens' SysConfLoader (Either a a))

getKey :: Companion a -> String
getKey (Companion a _ _) = a
getBody :: Companion a -> SysConf -> a
getBody (Companion _ b _) = view b
getBodyUpdater :: Companion a -> a -> SysConf -> SysConf
getBodyUpdater (Companion _ b _) x = set b x
getLoad :: Companion a -> SysConfLoader -> Either a a
getLoad (Companion _ _ c) = view c
getLoader :: Companion a -> a -> SysConfLoader -> SysConfLoader
getLoader (Companion _ _ c) x = set c (Right x)

cServerLifetimeSec :: Companion Int
cServerLifetimeSec = Companion "server-lifetime-seconds" _serverLifeTimeSeconds sclServerLifetimeSeconds
cServerPort :: Companion Port
cServerPort = Companion "server-port" _serverPort sclServerPort

data CompanionBox = forall a. (ToJSON a, FromJSON a) => CBox (Companion a)

boxInTheBox :: [CompanionBox]
boxInTheBox = [CBox cServerLifetimeSec, CBox cServerPort]


helperConfToJSON :: [CompanionBox] -> SysConf -> JSON.Value
helperConfToJSON lm v = object $ fmap (helperField v) lm
  where
    helperField :: SysConf -> CompanionBox -> Pair
    helperField v (CBox c) = (JSON.fromString $ getKey c) .= getBody c v

helperConfFromJSON :: String -> [CompanionBox] -> JSON.Value -> Parser SysConfLoader
helperConfFromJSON name lm =
  withObject name $
  \v -> do
    f <- foldlM (\s -> liftA2 (>>>) (pure s)) identity (fmap (helperField v) lm)
    pure $ f def
  where
    helperField :: JSON.Object -> CompanionBox -> Parser (SysConfLoader -> SysConfLoader)
    helperField v (CBox c) = maybe id (getLoader c) <$> v .:? (JSON.fromString $ getKey c)

helperConfFromLoader :: Monad m => [CompanionBox] -> (String -> m ()) -> (String -> m ()) -> SysConfLoader -> m SysConf
helperConfFromLoader lm l r scl = do
  f <- foldlM (\s -> liftA2 (>>>) (pure s)) identity (fmap (helperField l r scl) lm)
  pure $ f def
  where
    helperField :: Monad m => (String -> m ()) -> (String -> m ()) -> SysConfLoader -> CompanionBox -> m (SysConf -> SysConf)
    helperField l r v (CBox c) =
      merge l r (getKey c) (getBodyUpdater c) (getLoad c v)

merge :: Monad m
      => (s -> m ()) -> (s -> m ()) -> s -> (a -> b) -> Either a a -> m b
merge l r k f = 
  \case
    Left a -> l k >> return (f a)
    Right a -> r k >> return (f a)



instance HasRelatedFilename SysConf where
  relatedFilename _ = "system-conf.yaml"
instance ToJSON SysConf where
  toJSON = helperConfToJSON boxInTheBox

instance FromJSON SysConfLoader where
  parseJSON = helperConfFromJSON "SysConfLoader" boxInTheBox


loaderToConf :: Monad m => (String -> m ()) -> (String -> m ()) -> SysConfLoader -> m SysConf
loaderToConf =
  helperConfFromLoader boxInTheBox



serverLifeTimeMicroSeconds :: SysConf -> Int
serverLifeTimeMicroSeconds = (* 1000) . (* 1000) . serverLifeTimeSeconds


