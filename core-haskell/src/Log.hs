{-# LANGUAGE OverloadedStrings #-}

module Log ( prepareLogger, endLogger
           , debug, info, warn, err, fatal
           -- Reexport
           , Logger
           ) where

import System.Logger (Logger, defSettings, Level(..), setLogLevel, Output(..), setOutput, ToBytes(..), msg)
import qualified System.Logger as Logger


debug :: (ToBytes a, MonadIO m) => Logger -> a -> m ()
debug l = Logger.debug l . msg

info :: (ToBytes a, MonadIO m) => Logger -> a -> m ()
info l = Logger.info l . msg

warn :: (ToBytes a, MonadIO m) => Logger -> a -> m ()
warn l = Logger.warn l . msg

err :: (ToBytes a, MonadIO m) => Logger -> a -> m ()
err l = Logger.err l . msg

fatal :: (ToBytes a, MonadIO m) => Logger -> a -> m ()
fatal l = Logger.fatal l . msg


prepareLogger :: MonadIO m => m Logger
prepareLogger = do
  l <- Logger.new sTerminal
  info l ("Start Logger" :: Text)
  pure l
  where
   sTerminal = setLogLevel Debug . setOutput StdOut $ defSettings
--   sFile fname = setLogLevel Info . setOutput (Path fname) $ defSettings

endLogger :: MonadIO m => Logger -> m ()
endLogger l = do
  info l ("End Logger" :: Text)
  Logger.close l

