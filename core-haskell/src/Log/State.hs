module Log.State ( HasLogger(..)
                 , debug, info, warn, err, fatal
                 -- Reexport
                 , Logger
                 ) where

import qualified Log

import Control.Lens (Lens', use)
import System.Logger (ToBytes(..), Logger)


class HasLogger a where
  _logger :: Lens' a Logger


mold :: (HasLogger s, MonadState s m) => (Logger -> a -> m ()) -> a -> m ()
mold f a = do
  l <- use _logger
  f l a

debug :: (HasLogger s, MonadState s m, MonadIO m, ToBytes a) => a -> m ()
debug = mold Log.debug

info :: (HasLogger s, MonadState s m, MonadIO m, ToBytes a) => a -> m ()
info = mold Log.info

warn :: (HasLogger s, MonadState s m, MonadIO m, ToBytes a) => a -> m ()
warn = mold Log.warn

err :: (HasLogger s, MonadState s m, MonadIO m, ToBytes a) => a -> m ()
err = mold Log.err

fatal :: (HasLogger s, MonadState s m, MonadIO m, ToBytes a) => a -> m ()
fatal = mold Log.fatal
