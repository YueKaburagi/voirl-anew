#!/bin/bash

function cli() {
    http --json --print=Bhb localhost:1234 $@
}

ITSELF="${0##*/}"

case "$1" in
    #> list-all-save
    "list-all-save")
        cli "type=list-all-save"
        ;;
    #> new-game
    "new-game")
        cli "type=new-game"
        ;;
    #> save
    "save")
        cli "type=save"
        ;;
    #> load <index>
    "load")
        if [ $# -lt 2 ] ; then
            echo "USAGE: $ITSELF load <index>"
        else
            cli "type=load" "index=$2"
        fi
        ;;
    #B> list-adv
    "list-adv")
        cli "type=game" "game[cmd]=list-adventures"
        ;;
    #B> info-adv <adventure-ix>
    "info-adv")
        if [ $# -lt 2 ]; then
            echo "USAGE: $ITSELF info-adv <adventure-ix>"
        else
            cli "type=game" "game[cmd]=info-adventure" "game[adventure-ix]=$2"
        fi
        ;;
    #B> enter-adv <adventure-ix>
    "enter-adv")
        if [ $# -lt 2 ]; then
            echo "USAGE: $ITSELF enter-adv <adventure-ix>"
        else
            cli "type=game" "game[cmd]=enter-adventure" "game[adventure-ix]=$2"
        fi
        ;;
    #A> abort-adv
    "abort-adv")
        cli "type=game" "game[cmd]=abort-adventure"
        ;;
    #A> move <dir>
    "move")
        if [ $# -lt 2 ]; then
            echo "USAGE: $ITSELF move <direction>"
        else
            cli "type=game" "game[cmd]=move" "game[dir]=$2"
        fi
        ;;
    #D> debug-dump-cache
    "debug-dump-cache")
        cli "type=game" "game[debug]=dump" "game[type]=adv-cache"
        ;;
    #D> debug-dump-floor
    "debug-dump-floor")
        cli "type=game" "game[debug]=dump" "game[type]=adv-floor" "game[options][]=rooms" "game[options][]=pc" "game[options][]=chars"
        ;;
    *)
        echo "USAGE: $ITSELF <command>"
        echo "COMMANDS:"
        echo "  /top-scene"
        cat $0 | grep "#>" | grep -v sed | sed -e "s/\s*#>\s*/\t/"
        echo "  /game-scene/base-menu"
        cat $0 | grep "#B>" | grep -v sed | sed -e "s/\s*#B>\s*/\t/"
        echo "  /game-scene/adventure"
        cat $0 | grep "#A>" | grep -v sed | sed -e "s/\s*#A>\s*/\t/"
        echo ""
        echo "  /game-scene/(debug)"
        cat $0 | grep "#D>" | grep -v sed | sed -e "s/\s*#D>\s*/\t/"

        echo -e "\nNOTE:\n\tThis script requires HTTPie version 3.0.0 or later."
        ;;
esac
